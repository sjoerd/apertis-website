+++
title = "Applying Licensing"
weight = 100
aliases = [
	"/old-designs/latest/license-applying.html",
	"/old-designs/v2020/license-applying.html",
	"/old-designs/v2021dev3/license-applying.html",
	"/old-developer/latest/licensing.html",
	"/old-developer/v2019/licensing.html",
	"/old-developer/v2020/licensing.html",
	"/old-developer/v2021pre/licensing.html",
	"/old-developer/v2022dev0/licensing.html",
]
outputs = [ "html", "pdf-in",]
date = "2019-10-22"
+++

Apertis code, including build scripts, helpers and recipes, is primarily
licensed under the
[Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
Images (such as icons) and documentation in Apertis are licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
(CC BY-SA 4.0) license.

Apertis also makes use of other projects which may have other licenses, such as
the [GPL and LGPL](https://www.gnu.org/licenses/licenses.html). For example,
this includes projects such as the Linux kernel, WebKit and GLib.

You can find more information about the licensing for each component in
the `COPYING` files in each components repository.

When you contribute to any Apertis code repository, you are agreeing to license
your work under the same license as the rest of the code in the repository.

If you are contributing software components to Apertis, these should be
licensed under a
[suitable open source license]({{< ref "license-expectations.md" >}}). The
preferred license for Apertis is the MPL 2.0, unless there’s a good reason for
it to be licensed differently.

When building an application on top of Apertis (and do not intend to contribute
it back to the Apertis project) you can choose any license that you wish for
your application. However, care should be taken to ensure that your license is
compatible with any libraries that you may use provided by the Apertis
platform.

{{% notice warning %}}
You must ensure that you comply with all licensing terms when you use or
modify Apertis.
{{% /notice %}}

## Licensing of code

There are two parts to licensing a project:

* distribute the license text
* include license headers in each file

### Distribute the license file

The license text is normally distributed in the `COPYING.MPL` or `COPYING` file
which lives in the top directory in the git repository for the project. This
file will contain the full license text, as
[provided by Mozilla](https://www.mozilla.org/media/MPL/2.0/index.815ca599c9df.txt),
without any modifications or changes. For example, see the
[newport COPYING file](https://gitlab.apertis.org/pkg/target/newport/blob/apertis/v2019/COPYING).

While `COPYING` is a more common filename to use, `COPYING.MPL` accounts for
the case where there may be files in the project under a different license
which would require multiple `COPYING.*` files to be included. This case is
most common with applications which may include content such as logos, images
and documentation under different licenses.


#### Distributing portions under different licenses

It is very common to see only one `COPYING` file in a project which contains
only a single license text, and it is also common to see the images and
documentation shipped either under a license which is best suited for code
(that is to say, impossible for images and documentation to comply with) or
without proper licensing.

Licensing all parts of your project appropriately is not complicated and we
highly recommend that you do so. Your typical directory structure should look
something like:

```
<project>
↳COPYING
↳COPYING.MPL
```

The `COPYING` file should contain information about all parts of the project.
For example, it could look like:

```
<project> is an Apertis project and follows the licensing guidelines as
specified at https://www.apertis.org/designs/license-applying/".

Code
----
All code in this project is licensed under the Mozilla Public License Version
2.0. See COPYING.MPL for the full license text.

Images
------
All icons and other images in this project are licensed under CC BY-SA 4.0
International. For information about this license, see
https://creativecommons.org/licenses/by-sa/4.0/

Documentation
-------------
All documentation in this project is licensed under CC BY-SA 4.0 International.
For information about the license, see
https://creativecommons.org/licenses/by-sa/4.0/
```

Your `COPYING.MPL` should contain the full license text for the Mozilla Public
License Version 2.0. You may also need to have other license-specific `COPYING`
files, depending on your project.

In this case, we include a `COPYING.MPL` to comply with the MPL 2.0 as it
requires the full license text to be included in your project, but we do not
have a `COPYING.CC-BY-SA` because the CC BY-SA 4.0 license does not require the
license text to be distributed (but you may include it if you wish to do so).


### Add license headers to each file

It is good practice to include a `license header` to the top of a code file. It
is a comment that typically consists of a `copyright notice`, the SPDX license
identifier and a license blurb which is provided with the license. The license
header for a specific file must contain only copyright holders of content which
is in that file. This means that the license header in each of your project
files are likely to list different copyright holders.

The copyright notice will normally contain `Copyright ©` followed by the
copyright years and the copyright holder. It is recommended that you also
include a contact email address for the copyright holder, although this is
optional.

If you are employed to contribute to Apertis, the copyright holder may be
either you or your employer. We recommend that you check with your employer
before you contribute as it may not be possible to completely remove any
mistakes as the code is publicly available and archived.

This is what a typical MPL license header looks like:

```
/*
 * Copyright © 2015, 2016 Anita Developer <a.developer@example.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
```

For additional guidance on how license headers work, please read the
[GNU license guidance](https://www.gnu.org/licenses/gpl-howto.html). The
theory of using the MPL license headers is the same as for the GPL, but do keep
in mind that the GPL/GNU licenses have different content from the MPL license.


#### Copyright notice date range

The copyright notice should always correspond to the year that the work was
done in.

For example,

* work done in 2015, should have `© 2015`
* work done in 2016 should have `© 2016`
* files which had work done in 2015 and 2016 should have `© 2015–2016`
* files which had work done in 2014 and 2016 should have `© 2014, 2016`
* files which had work done in 2013, 2015 and 2016 should have `© 2013, 2015–2016`

Your copyright notice should normally look something like:

```
Copyright © 2016 Anita Developer <a.developer@example.com>
```

For documentation written in Mallard, you should use the `<credit>`, `<name>`,
`<email>` and `<years>` tags which will generate the correct copyright notice
automatically.

The copyright holder will normally be you or, if you make the contribution as
part of paid work, then your employer. If you are unsure about this, you should
check what your employment contract states on the matter or seek further legal
advice.

You must not amend copyright notices which are inserted by other people without
their explicit permission which must be recorded appropriately.

Apart from the license header, you should also include the
[vim modeline at the top of the file]( {{< ref "coding_conventions.md#code-formatting" >}} )
to help enforce consistent coding style.


### Add a new code file to a project

Each code file in all Apertis repositories must contain the license header.
This license header must be added in the commit when the file is first added to
the project and will typically contain your copyright notice.

Always double check the project license before adding a license header: not all
projects are licensed under the MPL! You can find the project license in the
`COPYING` or `COPYING.*` files. This is most likely to be the case for
repositories which are upstream projects that have Apertis specific
customisations applied to them. If unsure, do ask the project maintainer for
help. You can find the list of maintainers in the .doap file in the project git
repository.


### Make changes to an existing code file

When you make a copyrightable change to a file in an existing project, you will
need to add your copyright notice to the existing copyright header, but make
sure that you do not amend or change the license notice in any way! Add your
notice below the existing copyright notices, but above the license notice.

For example, if your copyright notice was
`Copyright © 2016 Andrew Contributor <a.contributor@example.com>` then the
resulting copyright header would look like:

```
/*
 * Copyright © 2015, 2016 Anita Developer <a.developer@example.com>
 * Copyright © 2016 Andrew Contributor <a.contributor@example.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
```

## License for images

As with code, there are two parts to licensing your images:

* include mention of the image licensing in the COPYING (recommended) or README
  file as covered in `Distributing portions under different licenses`
* add the license to the image metadata in case it becomes separated from the
  repository

### Add the license to the metadata

You can use `exiv2`, which is a command-line tool, to write Exif metadata into
the file. `exiv2` should be available through your Linux distribution or you
can [download](http://www.exiv2.org/download.html) it for Linux or Windows from
its website.

For example, if your copyright notice is
`© 2016 Alice Artist <a.artist@example.com>` then this command will add it to
the `Exif.Image.Copyright` key:
```
exiv2 -v -M"set Exif.Image.Copyright Copyright © 2016 Alice Artist <a.artist@example.com>. This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA." <path to your image>
```

Replace the copyright notice with your own and replace `<path to your image>`
with the path to the image which you want to update.

You can now check the copyright notice with:
```
exiv2 <path to your image>
```

Which will output something that looks like:
```
File name       : apertis-icon.png
File size       : 1228 Bytes
MIME type       : image/png
Image size      : 36 x 36
Thumbnail       : None
Copyright       : Copyright © 2016 Alice Artist <a.artist@example.com>. This wor
k is licensed under the Creative Commons Attribution-ShareAlike 4.0 Internationa
l License. To view a copy of this license, visit <nowiki>http://creativecommons.org/lice</nowiki>
nses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View
, CA 94042, USA.
Exif comment    :
```
There may be some other tags present in the output.

