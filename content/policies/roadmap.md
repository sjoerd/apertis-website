+++
date = "2020-06-18"
weight = 100

title = "Roadmap"

aliases = [
    "/old-wiki/Roadmap"
]
+++

This is a rough list of areas which are planned to be worked on in releases in
the near future.  This is not a full list and is intended to be used only as a
rough guide and is subject to change.

# 2020

## Q3

### Concepts

- Use of Eclipse hawkBit for Apertis updates
- Addition of NTFS support for offline updates

### HMI

- Import of agl-compositor as a reference HMI

### Infrastructure

- Move project health checks from Jenkins to GitLab CI pipelines
- Evaluate need for further testing and monitoring of Apertis infrastructure
- Move tiny-image-recipe build pipeline from Jenkins to GitLab CI

### System Update

- Integration of Eclipse hawkBit as update option


## Q2

### Concepts

- Integration of OP-TEE
- Automated OSS compliance
- Addition of exFAT support for offline updates

### Documentation

- Integrated search on www.apertis.org

### Infrastructure

- Move test binary infrastructure from Jenkins to GitLab CI pipeline
- Feedback from OBS builds in GitLab CI pipeline
- Investigation of emulated builds for non x86-64 architectures

### System Update

- Signing of offline updates
- Encrypted offline updates

## Q1

### Applications

- Add initial flatpak support

### Documentation

- Release management, long-term support and build reproducability
- Replace Apertis wiki with GitLab pages website

### Infrastructure

- Move image building pipelines from Jenkins to Gitlab CI
- Move docker container generation from Jenkins to GitLab CI pipeline
- User Mode Linux backend to Debos
- Move to hosting all Apertis packages in GitLab repositories

### Security

- Secure boot for i.MX6

### Upstream

- Submit pstore support to U-Boot


# 2019

### SDK

- Support artifacts for NFS boot as part of the official release
- Cross-building: Deliver a compiler toolchain tarball
- Implementation of solution for maintaining workspace across SDK update

### System Update

- OTA system updates (hosting and device agent)
- Add signature mechanism to upstream OSTree avoiding GPLv3 dependencies
- System update authentication checks (signatures)
- Deployment management proof-of-concept: Infrastructure
- Deployment management proof-of-concept: Integration with image build
- Deployment management proof-of-concept: Apertis hawkBit Agent

### Infrastructure

- Initial investigation on using GitLab CI to submit packages to OBS
- Implement the improved branching automation for OBS
- Improved QA infrastructure: manual test results

### Concepts

- Concept document for the next-generation Apertis application framework
- Concept for hosting the whole build infrastructure on Intel x86-64-only
  providers
- Concept for secure boot
- Concept for improving the branching automation

### Documentation

- Design document for OTA system updates
- Developer documentation for sysroot and devroot usage
- Document how to track DUT-local resources consumption during tests
