+++
title = "Apertis License exceptions"
short-description = "Document license exceptions for projects in Apertis"
weight = 100
aliases = [
	"/old-designs/latest/license-exceptions.html",
	"/old-designs/v2019/license-exceptions.html",
	"/old-designs/v2020/license-exceptions.html",
	"/old-designs/v2021dev3/license-exceptions.html",
]
outputs = [ "html", "pdf-in",]
date = "2019-04-16"
+++

# License exceptions

License exceptions for Apertis are listed below.
Each exception must provide the following informations:

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>The project name</td>
</tr>
<tr>
  <th>component</th>
  <td>The repository components apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>The date at which the exception was added to this document</td>
</tr>
<tr>
  <th>validator</th>
  <td>The name of the person who validated the exception</td>
</tr>
<tr>
  <th>rule</th>
  <td>The rules that are ignored by this exception</td>
</tr>
<tr>
  <th>reason</th>
  <td>A description of why the exception is granted and makes sense</td>
</tr>
</table>

## gcc-8

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>gcc-8</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>April 17, 2019</td>
</tr>
<tr>
  <th>validator</th>
  <td>fredo</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
    <p>The GCC source package is granted exception to be present in target repository component
    because it produces binary packages covered by different licensing terms:</p>
    <ul>
      <li>the compiler packages are released under the GPL-3</li>
      <li> the <code>libgcc</code> runtime library is covered by the
        <a href="https://www.gnu.org/licenses/gcc-exception-3.1-faq.html">GCC Runtime Library Exceptions</a>
      </li>
    </ul>
    <p>Programs compiled with GCC link to the <code>libgcc</code> library to implement some compiler intrinsics,
    which means that the <code>libgcc</code> must live in the <code>apertis:*:target</code> component
    since it is a direct runtime dependency of packages in the same component.</p>
    <p>For this reason, an exception is granted to the <code>gcc</code> source package
    on the ground that:</p>
    <ul>
      <li>code that is shipped on target devices (that is, <code>libgcc</code>) is covered by the
        <a href="https://www.gnu.org/licenses/gcc-exception-3.1-faq.html">GCC Runtime Library Exceptions</a>
      </li>
      <li>the pure GPL-3 code is not meant to be shipped in target devices</li>
    </ul>
  </td>
</tr>
</table>

## libtool

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>libtool</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>August 05, 2019</td>
</tr>
<tr>
  <th>validator</th>
  <td>ritesh</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
   libtool is granted exception to be present in target repository component<br/>
   because all the source files are licensed under the GPLv2 with the exception<br/>
   of build files, which are licensed under GPLv3.<br/>
   These build files are used only to build the binary package and are not<br/>
   GPLv3 violations for the built binary packages.<br/>
  </td>
</tr>
</table>

## elfutils

<table>
<colgroup>
  <col style="width: 20%" />
  <col style="width: 80%" />
</colgroup>
<tr>
  <th>project</th>
  <td>elfutils</td>
</tr>
<tr>
  <th>component</th>
  <td>apertis:*:target</td>
</tr>
<tr>
  <th>date</th>
  <td>September 17, 2019</td>
</tr>
<tr>
  <th>validator</th>
  <td>andrewsh</td>
</tr>
<tr>
  <th>rule</th>
  <td>No GPL v3</td>
</tr>
<tr>
  <th>reason</th>
  <td>
   <p><code>elfutils</code> is software dual-licensed as LGPL-3+ or GPL-2+, which
   means that any combined work using it has to be shipped under terms
   compatible with either of those two licenses. To avoid the effects of the
   GPL-3 provisions as required for the <code>target</code> repository, any
   combined work depending on any of the libraries provided by
   <code>elfutils</code> must be effectively licensed under the GPL-2 terms.
   </p>
   <p>
   The following binary packages are linking against <code>elfutils</code> in
   way that no GPL-3 restrictions need to be applied as they only ship
   executables that produce combined works under the GPL-2:
   </p>
   <ul>
     <li><code>linux-perf-4.19</code>: GPL-2, does not ship libraries,
         development tool not meant to be shipped on products
     <li><code>linux-kbuild-4.19</code>: GPL-2, does not ship libraries,
         development tool not meant to be shipped on products
     <li><code>bluez</code>: GPL-2, does not ship libraries
     <li><code>libglib2.0-bin</code>: LGPL-2.1, effectively combined to GPL-2,
         does not ship libraries
   </ul>
   <p>
   In addition, the <code>mesa</code> source package produces binary packages
   containing drivers that need to be linked to <code>libelf</code> and, in
   turn, get linked to graphical applications. This would impose LGPL-3+
   restrictions on <code>libelf</code> unless the application and all the other
   linked libraries can be combined as a GPL-2 work. This is not an acceptable
   restriction, so the affected drivers have been disabled, and no binary
   package produced from the <code>mesa</code> source package links to any
   library shipped by <code>elfutils</code>.
   </p>
  </td>
</tr>
</table>
