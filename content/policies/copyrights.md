+++
date = "2015-11-02"
weight = 100

title = "Copyrights"

aliases = [
    "/old-wiki/Apertis:Copyrights"
]
+++

The text of the Apertis website is copyrighted (automatically, under the
Berne Convention) by Apertis editors and contributors and is formally
licensed under Creative Commons Attribution-ShareAlike 4.0 Unported
License (CC BY-SA) unless explicitly stated otherwise.

The licenses that the Apertis website uses grant free access to our content
in the same sense that free software is licensed freely. Apertis website
content can be copied, modified, and redistributed *if and only if* the
copied version is made available on the same terms to others and
acknowledgment of the authors of the Apertis website article used is
included (a link back to the article is generally thought to satisfy the
attribution requirement; see below for more details). Copied Apertis
website content will therefore remain *free* under appropriate license and
can continue to be used by anyone subject to certain restrictions, most
of which aim to ensure that freedom. This principle is known as
**copyleft** in contrast to typical copyright licenses.

The English text of the CC BY-SA license is the only legally binding
restriction between authors and users of the Apertis website content.

## Contributors' rights and obligations

If you contribute text directly to the Apertis website, you thereby license
it to the public for reuse under CC BY-SA. Non-text media may be
contributed under a variety of different licenses that support the
general goal of allowing unrestricted re-use and re-distribution.

If you want to import text that you have found elsewhere or that you
have co-authored with others, you can only do so if it is available
under terms that are compatible with the CC BY-SA license.

You retain copyright to materials you contribute to the Apertis website,
text and media. Copyright is *never* transferred to Apertis. You can
later republish and relicense them in any way you like. However, you can
never retract or alter the license for copies of materials that you
place here; these copies will remain so licensed until they enter the
public domain when your copyright expires (currently some decades after
an author's death).

### Guidelines for images and other media files

Images, photographs, video and sound files, like written works, are
subject to copyright. Images, video and sound files on the internet need
to be licensed directly from the copyright holder or someone able to
license on their behalf.

### Re-use of text

  - Attribution: To re-distribute text from the Apertis website in any
    form, provide credit to the authors either by including a) a
    hyperlink (where possible) or URL to the page or pages you are
    re-using, b) a hyperlink (where possible) or URL to an alternative,
    stable online copy which is freely accessible, which conforms with
    the license, and which provides credit to the authors in a manner
    equivalent to the credit given on this website, or c) a list of all
    authors. (Any list of authors may be filtered to exclude very small
    or irrelevant contributions.)
    Copyleft/Share Alike: If you make modifications or additions to the
    page you re-use, you must license them under the Creative Commons
    Attribution-Share-Alike License 4.0 or later.
    Indicate changes: If you make modifications or additions, you must
    indicate in a reasonable fashion that the original work has been
    modified. If you are re-using the page in a website, for example,
    indicating this in the page history is sufficient.
    Licensing notice: Each copy or modified version that you distribute
    must include a licensing notice stating that the work is released
    under CC BY-SA and either a) a hyperlink or URL to the text of the
    license or b) a copy of the license. For this purpose, a suitable
    URL is: <http://creativecommons.org/licenses/by-sa/4.0/>

**For further information**, please refer to the [legal code of the CC
BY-SA License](http://creativecommons.org/licenses/by-sa/4.0/legalcode).

## Notes

This page applies only to the Apertis website. The Apertis code is licensed
separately and the licenses can be found in the code repositories,
normally in a file called COPYING.

## Credits

*The first version of this page was based on
<https://en.wikipedia.org/wiki/Wikipedia:Copyrights>*
