+++
short-description = "Deployment management: How to install and use hawkBit for Apertis"
license = "CC-BY-SAv4.0"
title = "Deployment Management using hawkBit"
weight = 100
aliases = [
    "/old-developer/latest/deployment-management.html",
    "/old-developer/v2019/deployment-management.html",
    "/old-developer/v2020/deployment-management.html",
    "/old-developer/v2021pre/deployment-management.html",
    "/old-developer/v2022dev0/deployment-management.html",
]
date = "2019-10-09"
+++

The Apertis distribution provides a number of routes for managing updates to
deployed devices. This document discusses utilising
[hawkBit](https://www.eclipse.org/hawkbit/), a domain independent back-end
framework for rolling out software updates.

# Design

The **hawkBit data model** defines a hierarchy of software that starts with a
distribution, which can have (sub-)modules and these may have multiple
artifacts.  The hawkBit Data Model concept can be found
[here](https://www.eclipse.org/hawkbit/concepts/datamodel/).

For Apertis, we want the device agent to be as simple as possible. So it should
only need to check the hawkBit server for an update, download it and trigger
installation.

Apertis uses OSTree to provide atomic update management. As part of the
[automated image generation](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/#gitlab-cicd-automation)
_OSTree static delta_ files are created which can be used to
[update existing OSTree installations]({{< ref "guides/ostree.md#offline-update" >}}).

When an _OSTree static delta_ file has been created, a new _distribution_
containing a single _module_ for each build and for each Apertis release, type
and architecture is created.

The _OSTree static delta_ file is uploaded to the _module_ as its single artifact.

_Distribution_ and _module_ version are based on the _image_ version.

To be more concrete, when the image pipeline is building the `armhf` minimal
image it also builds a matching _full static OSTree bundle_ named, for
instance, `apertis_ostree_v2019-minimal-armhf-uboot_v2019.4.delta`.

This bundle is uploaded to hawkBit under the
`apertis_v2019-minimal-armhf-uboot` module with version `v2019.4`. This
_module_ is linked to a _distribution_ sharing the same _name_ and _version_.

# HawkBit backend installation

The `hawkBit update server` can be installed using docker-compose (cf.
[HawkBit Getting Started](https://www.eclipse.org/hawkbit/gettingstarted/)).

It needs a **docker-compose.yml** and an **application.properties** file to
configure the hawkBit server.

**docker-compose.yml** file (based on
[docker-compose.yml](https://github.com/eclipse/hawkbit/blob/master/hawkbit-runtime/docker/docker-compose.yml)):

```
#
# Copyright (c) 2018 Bosch Software Innovations GmbH and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
version: '3'

services:

  # ---------------------
  # MySQL service
  # ---------------------
  mysql:
    image: "mysql:5.7"
    environment:
      MYSQL_DATABASE: "hawkbit"
      MYSQL_USER: "root"
      MYSQL_ALLOW_EMPTY_PASSWORD: "true"
    restart: always
    ports:
      - "3306:3306"
    labels:
      NAME: "mysql"

  # ---------------------
  # HawkBit service
  # ---------------------
  hawkbit:
    image: "hawkbit/hawkbit-update-server:latest-mysql"
    environment:
      - 'SPRING_DATASOURCE_URL=jdbc:mysql://mysql:3306/hawkbit'
      - 'SPRING_DATASOURCE_USERNAME=root'
    restart: always
    ports:
      - "8080:8080"
    volumes:
      - ./application.properties:/opt/hawkbit/application.properties
    labels:
      NAME: "hawkbit"
```

**application.properties** file:
```
#
# Copyright (c) 2015 Bosch Software Innovations GmbH and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#

# User Security
spring.security.user.name=admin
spring.security.user.password={noop}admin
spring.main.allow-bean-definition-overriding=true

# DDI authentication configuration
hawkbit.server.ddi.security.authentication.anonymous.enabled=false
hawkbit.server.ddi.security.authentication.targettoken.enabled=true
hawkbit.server.ddi.security.authentication.gatewaytoken.enabled=true

# Optional events
hawkbit.server.repository.publish-target-poll-event=false

## Configuration for DMF/RabbitMQ integration
#spring.rabbitmq.username=guest
#spring.rabbitmq.password=guest
#spring.rabbitmq.virtual-host=/
#spring.rabbitmq.host=localhost
#spring.rabbitmq.port=5672

# Define own users instead of default "admin" user:
hawkbit.server.im.users[0].username=apertis
hawkbit.server.im.users[0].password={noop}Apertis!
hawkbit.server.im.users[0].firstname=Apertis
hawkbit.server.im.users[0].lastname=HawkBit
hawkbit.server.im.users[0].permissions=ALL

# Enable CORS and specify the allowed origins:
#hawkbit.server.security.cors.enabled=true
#hawkbit.server.security.cors.allowedOrigins=http://localhost
```

Copy them to a directory and run:

    $ docker-compose up -d

You can then connect to the hawkBit Management UI at server address and port 8080, with username `apertis` and password `Apertis!`.

# Trigger a deployment from Apertis hawkBit update server

## Deployment setup

The deployment of OSTree static delta files should be performed as part of the
automated Apertis image build process, but can also be done manually.

### Manually updating static delta to update server

#### Using the UI

##### Create a _Software Module_ in Upload page
This allows to group multiple artifacts.

Click on **+** in _Software Module_ panel and provide `Type`, `Name`, and `Version`.

Multiple artifacts can be attached to the _Software Module_ using the `upload File` button.

##### Create a _Distribution_ in Distributions page
The distribution is a collection of _software module_(s).

1st create the _Distribution_ (**+** sign in Distributions panel), which will require `Name` and `Version` info, then drag-and-drop required _Software Module_(s) on top of the newly created _Distribution_.

#### Using the API
1st we need to define some variables which will be used: `name`, `description`, `version` and the `delta_file` to upload, e.g.:

    $ name="apertis_test-minimal-armhf-uboot"
    $ description="Apertis test minimal for armhf uboot"
    $ version="v1.0"
    $ delta_file="apertis_ostree_minimal-armhf-uboot.delta"

##### Create a _Software Module_
    $ curl --user admin:admin 'http://localhost:8080/rest/v1/softwaremodules' -i -X POST -H 'Content-Type: application/json;charset=UTF-8' -d '[ {"vendor" : "Apertis","name" : "$name","description" : "$description", "type" : "os", "version" : "$version" } ]'
    [{"createdBy":"admin","createdAt":1568627145649,"lastModifiedBy":"admin","lastModifiedAt":1568627145649,"name":"apertis_test-minimal-armhf-uboot","description":"Apertis test minimal for armhf uboot","version":"v1.0","type":"os","vendor":"Apertis","deleted":false,"_links":{"self":{"href":"http://localhost:8080/rest/v1/softwaremodules/1"}},"id":1}]

##### Upload an _artifact_ to a _Software Module_
This will use the `href` link returned during the _Software Module_ creation.

    $ curl --user admin:admin 'http://localhost:8080/rest/v1/softwaremodules/1/artifacts' -i -X POST -H 'Content-Type: multipart/form-data' -F 'file=@$delta_file'
    {"createdBy":"admin","createdAt":1568627195280,"lastModifiedBy":"admin","lastModifiedAt":1568627195280,"hashes":{"sha1":"d3b582709d7574e92e16671c5f097fda7795f832","md5":"c368c561a9279f60f7a1b04eabf63d58"},"providedFilename":"apertis_ostree_minimal-armhf-uboot.delta","size":21,"_links":{"self":{"href":"http://localhost:8080/rest/v1/softwaremodules/1/artifacts/1"},"download":{"href":"http://localhost:8080/rest/v1/softwaremodules/1/artifacts/1/download"}},"id":1}

##### Create a _Distribution_
the will use the module `id` returned during the _Software Module_ creation.

    $ curl --user admin:admin 'http://localhost:8080/rest/v1/distributionsets/' -i -X POST -H 'Content-Type: application/json;charset=UTF-8' -d '[ {"requiredMigrationStep" : false,"name" : "$name","description" : "$description", "type" : "os", "version" : "$version", "modules" : [ {"id" : 1} ] } ]'
    [{"createdBy":"admin","createdAt":1568627754940,"lastModifiedBy":"admin","lastModifiedAt":1568627754940,"name":"apertis_test-minimal-armhf-uboot","description":"Apertis test minimal for armhf uboot","version":"v1.0","modules":[{"createdBy":"admin","createdAt":1568627145649,"lastModifiedBy":"admin","lastModifiedAt":1568627195295,"name":"apertis_test-minimal-armhf-uboot","description":"Apertis test minimal for armhf uboot","version":"v1.0","type":"os","vendor":"Apertis","deleted":false,"_links":{"self":{"href":"http://localhost:8080/rest/v1/softwaremodules/1"}},"id":1}],"requiredMigrationStep":false,"type":"os","complete":true,"deleted":false,"_links":{"self":{"href":"http://localhost:8080/rest/v1/distributionsets/1"}},"id":1}]

### Deployment to target(s)

There is 2 ways two start the distribution deployment: manually for a specific target, or to multiple targets using a filter.

#### Manually deploy to one target

From the **Deployment** page, simply drag-n-drop the _distribution_ on top of the desired _target_.

#### Rollout deployment to multiple targets

This needs to create a filter in **Target Filters** page by clicking on **+** sign, and providing a `Name`, the `filter string` (e.g. `attribute.isoCode==DE`), the `Action type` and the `Start type`.
- `Action type` is related to how the target should manage the update:
 - forced: immediately
 - soft: based on user approval or device update time plan
 - time forced: soft then forced after a specific time
 - download only: device is supposed to only download the update and not install it
- `Start type` is related to the rollout:
 - manual: starts on user action
 - auto: starts immediately after rollout is created
 - scheduled: start as soon as rollout is ready and the set time has passed

:exclamation: Do not forget to save the filter.

Then, we are now able to create the deployment in **Rollout** page by clicking on **+** sign, giving it a `Name` and selecting the _Distribution_, _Filter_ and the number of group to use.

# Target device

## Target authentication

Target devices are identified in hawkBit by there `Controller Id` and `Target Token`.
Any `Controller Id` can be chosen, but should be unique.


A `Target token` is generated automatically for each new device added using the UI.

A device can be added manually in the hawkBit management UI, or multiple devices can be added by uploading a csv file containing:

```
Controller_Id_1,targetName1
Controller_Id_2,targetName2
…
```

A device can also be added using the [Management API](https://www.eclipse.org/hawkbit/apis/management_api/), allowing to set the `Target Token`:

    $ curl --user 'admin:admin' 'http://localhost:8080/rest/v1/targets' -i -X POST -H 'Content-Type: application/json;charset=UTF-8' -d '[ {"securityToken" : "2345678DGGDGFTDzztgf","address" : "https://192.168.0.1","controllerId" : "Controller_Id_1","name" : "Controller 1","description" : "Test controller 1"} ]' -v


## apertis-hawkbit-agent

The [apertis-hawkbit-agent](https://gitlab.apertis.org/appfw/apertis-hawkbit-agent) is a component in the OSTree-based images to poll
hawkBit update server, and in case of an update is available to download it
and trigger `apertis-update-manager` to apply it.

### Installation

Add `apertis-hawkbit-agent` package using `apt` action to debos image.

### Configuration

The `server URL`, `Controller ID` and `Target Token` are mandatory and are located in `/etc/apertis-hawbit-agent.ini` on the device, they should be uncommented and set to their respective values.

`Controller Id` and `Target Token` can be retrieved from _Target details_ in Hawkbit HMI deployment page.

The final configuration file should look like:
```
[server]
base-uri=http://<IP_address_or_URL_to_HawkBit_server>:8080
controller-id=<ControllerID>
key-token=<DeviceToken>
```

The `apertis-hawkbit-agent` should be enabled by running:

    $ sudo systemctl enable apertis-hawkbit-agent

After reboot, the device should be able to connect to hawkBit update server and retrieve updates.

# Possible improvements

* Stabilizing the hawkBit instance
* Optimize resource consumption by moving away from full static bundles
* Use hawkBit _tags_  for the _distributions_, setting _type_ and _architecture_. This will allow to easily find the right distribution to send to a specific target or set of targets.
* Improve _Apertis hawkBit agent_ interaction with _Apertis Update Manager_ by calling the `RegisterSystemUpgradeHandler` DBus method, and calling `ApplySystemUpgrade` when all artifacts has been applied, which may happen with partial upgrades.
* Send _successful_ feedback from _Apertis hawkBit agent_ to _hawkBit update server_ only after reboot of the upgraded system went well.
