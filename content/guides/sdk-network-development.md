+++
short-description = "Use the SDK virtual machine for network management and boot"
license = "CC-BY-SAv4.0"
title = "Configuring the SDK as a Network-based Development Server"
weight = 100
aliases = [
    "/old-developer/latest/vm-netboot-server.html",
    "/old-developer/v2019/vm-netboot-server.html",
    "/old-developer/v2020/vm-netboot-server.html",
    "/old-developer/v2021pre/vm-netboot-server.html",
    "/old-developer/v2022dev0/vm-netboot-server.html",
]

date = "2019-10-23"
+++

It is often advantageous to be able to test software builds on a target system
by booting them over a network, typically booting from artifacts shared via
TFTP or NFS. It may not always be viable or sensible to arrange for such
resources to be exposed on a shared network. The Apertis SDK can be utilised to
provide such services and be configured to share them with a target board with
minimal extra infrastructure required.

The following services are typcially required in such a development
environment:

* DHCP -- IP addresses management
* DNS -- name server
* NTP -- time settings
* TFTP -- boot over the network support (for netboot only)
* NFS -- network file system (for netboot only)

# Preconditions

* Apertis SDK VM installed, as described in [installation guide]( {{< ref
  "virtualbox.md" >}} ).
* A USB Network Adapter is available to be used as additional network interface, dedicated for use with the target board.

  {{% notice warning %}}
  VirtualBox is limited to operating as a USB 1.1 host unless the "Oracle VM
  VirtualBox Extension Pack" is installed. To ensure that the network
  functions and is stable, either install the extension pack or attach
  only USB 1.1 network adapters to the VM.
  {{% /notice %}}

# USB Device Configuration

To minimise any influence from the VM host and isolate the network connection between the host and development board, the USB Network Adapter should be configured so that it is handled directly by the virtual machine as a USB device. This provides the Apertis SDK with full control over the dedicated network device:

- In the VM settings, select the `USB` menu entry.

  ![](/images/vm-usbnet-setup.png)

- The "Add" icon (blue plug with a super imposed green plus) can be used to
  create a filter based on an existing plugged in USB device.
- The USB Network Adapter is attached as a USB device into the VM and should be
  physically connected to development board:

  ![](/images/vm-usbnet.png)

  {{% notice tip %}}
  If you want to attach several boards in the same time, a network switch can
  be connected between the USB network adapter and multiple development boards.
  {{% /notice %}}

* Check the network interfaces after the SDK VM boot:

      $ connmanctl services
      *AO Wired                ethernet_525400233200_cable
      *AR Wired                ethernet_5254000b00b5_cable

  In the above example, 2 network interfaces are shown. One will be the main
  virtual network interface to the VM, the other the USB network adapter. At
  the beginning of each line we get a few characters showing the status of the
  device, here `*AO` and `*AR` as detailed in the
  [connman documentation](https://01.org/connman/documentation):

  > The symbols in the output above are: '*' favorite (saved) network, 'A'
  > autoconnectable, 'O' online and 'R' ready. If no letter is shown in the O/R
  > column, the network is not connected.

  From this we can surmise that the second service,
  `ethernet_5254000b00b5_cable` is `ens4` (as shown in the diagram above) as it
  is yet to be configured and thus not online.

# Assign static address for the USB Network Adapter

In order to act as the service providing DHCP to the target device, we should
assign the interface on the VM a static IP. In this document we use the
dedicated network `192.168.42.0/24` for connection between the SDK VM and
target device. We will assign address `192.168.42.1` to the `ens4` interface on
SDK VM:

    sudo connmanctl config ethernet_5254000b00b5_cable --ipv4 manual 192.168.42.1 255.255.255.0

# Firewall and network forwarding setup

Check the interface name which is dedicated for devices management with command
`ip addr show` -- you will see the correct interface name with address
`192.168.42.1` (`ens4` in the examples here).

* Add rules allowing to access to DNS, DHCP and NTP services on VM:

      sudo iptables -D INPUT -j REJECT --reject-with icmp-host-prohibited
      sudo iptables -A INPUT -p udp --dport 67 -i ens4 -j ACCEPT
      sudo iptables -A INPUT -p udp --dport 53 -i ens4 -j ACCEPT
      sudo iptables -A INPUT -p tcp --dport 53 -i ens4 -j ACCEPT
      sudo iptables -A INPUT -p udp --dport 123 -i ens4 -j ACCEPT
      sudo iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited

* Allow access to outer network for development board:

      sudo iptables -D FORWARD -j REJECT --reject-with icmp-host-prohibited
      sudo iptables -A FORWARD -i ens4 -j ACCEPT
      sudo iptables -A FORWARD -o ens4 -j ACCEPT
      sudo iptables -A FORWARD -j REJECT --reject-with icmp-host-prohibited
      sudo iptables -t nat -A POSTROUTING --src 192.168.42.0/24 -j MASQUERADE

* Save iptables configuration with command:

      sudo iptables-save | sudo tee /etc/iptables/rules.v4

* Allow forwarding

      sysctl -w net.ipv4.ip_forward=1

  And add the same into the system's configuration to enable forwarding
automatically after reboot:

      echo "net.ipv4.ip_forward=1" | sudo tee /etc/sysctl.d/enable-forward.conf

# Install additional packages

Here we use [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) lightweight server to provide DNS, DHCP and TFTP services.
Also we need to provide a correct time with [NTP](http://www.ntp.org/documentation.html) server for development boards without backup battery allowing to use HTTPS/TLS connections and OTA upgrades.

{{% notice note %}}
If the proxy server is needed to access to outer network then need to add it into the APT configuration file `/etc/apt/apt.conf.d/90proxy`:
```
Acquire::http::Proxy "http://10.168.128.45:3128";
Acquire::https::Proxy "http://10.168.128.45:3128";
```
{{% /notice %}}

Install additional packages on the SDK VM:

    sudo apt-get update
    sudo apt-get install -y dnsmasq ntp

# Configure DNS and DHCP

By default `dnsmasq` works as caching DNS service allowing the name resolution for managed devices
Add minimal configuration file `/etc/dnsmasq.d/10-dhcp.conf` for address management:

```
listen-address=192.168.42.1
bind-interfaces
dhcp-range=192.168.42.10,192.168.42.200,10m
dhcp-option=option:ntp-server,192.168.42.1
```

The last option forces to use SDK VM for time synchronization.

Ensure dnsmasq is enabled and restart the service with new configuration:

    sudo systemctl enable dnsmasq
    sudo systemctl restart dnsmasq

# Configure NTP service

Minimal working configuration file `/etc/ntp.conf` for the system without access to external time sources. Omit the drift file since for VM it doesn't help.

```
# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer noquery limited
restrict -6 default kod notrap nomodify nopeer noquery limited

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict -6 ::1

# Use ourselves as single source
server 127.127.1.0 prefer iburst
# Allow divices to use our time server
restrict 192.168.42.0 mask 255.255.255.0 nomodify notrap
```

Ensure the service is enabled and restart it with new configuration:

    sudo systemctl enable ntp
    sudo systemctl restart ntp

# Enable TFTP

Make a directory to be used as the TFTP root:

    sudo mkdir /srv/tftp

Ensure the following options are enabled in the dnsmasq config file
`/etc/dnsmasq.conf`, the first is likely to be commented out with a leading `#` that needs removing. The tftp root option is likely to need enabling and altering to point to the above created directory:

```
enable-tftp
tftp-root=/srv/tftp
```

Restart the service with new configuration:

    sudo systemctl restart dnsmasq

# Enable NFS

Install the required NFS kernel server package:

    sudo apt update
    sudo apt install nfs-kernel-server

Make a directory to be used as the NFS root:

    sudo mkdir /srv/nfs

Add an entry in `/etc/exports` to export this directory, configuring it so that
it can be used for NFS root file systems to the network we are using to connect
target devices:

```
/srv/tftp	192.168.42.1/24(rw,sync,no_subtree_check,no_root_squash)
```

Run the following command to update the kernels table of NFS exports:

    sudo exportfs -a

# Hints

- If the ethernet USB dongle stops working on the SDK when the target board
  reboots, you may be able to workaround this by connecting both the SDK USB
  dongle and the target to an ethernet hub/switch to keep the carrier signal
  stable on the SDK side even when the target board reboots.

- If you wish to connect multiple target devices and only have a router instead
  of a switch, you may be able to get this to work by disabling any services
  which may affect connectivity like the DHCP server on the router. It's also
  worth disabling any other service which is not needed for this use, like
  wireless access.
