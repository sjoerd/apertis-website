+++
date = "2015-09-08"
weight = 100

title = "Video Animation on Boot"

aliases = [
    "/docs/f.mme.11-video-animation-on-boot.md",
    "/old-wiki/Docs/F.MME.11-video-animation-on-boot",
    "/old-wiki/Docs/video-animation-on-boot"
]
+++

Apetis uses [Plymouth](http://www.freedesktop.org/wiki/Software/Plymouth/) to
implement the animation on boot.

To customize its boot animation, Plymouth has support to define a set of splash
themes.

From the Plymouth [README](http://cgit.freedesktop.org/plymouth/plain/README)
file:

"Plymouth supports various "splash" themes which are analogous to screensavers,
but happen at boot time. There are several sample themes shipped with plymouth,
but most distributions that use plymouth ship something customized for their
distribution."

You can list the available themes on your system by executing:

    $ sudo plymouth-set-default-theme --list

And can change the default theme with:

    $ sudo plymouth-set-default-theme <theme>

where `<theme>` is your selected theme (i.e: spinner)

After changing the default theme the initial ram filsystems has to be
regenerated to add this information:

    $ sudo update-initramfs -u

## Plymouth sample splash themes

The list of [samples themes](http://cgit.freedesktop.org/plymouth/tree/themes)
includes 2 text based themes and 7 graphical ones:

  - Text: "Text mode theme with tricolor progress bar"
  - Details: "Verbose fallback theme"
  - Fade-in: "Simple theme that fades in and out with shimmering stars"
  - Glow: "Corporate theme with pie chart boot progress followed by a glowing
    emerging logo"
  - Script: "Script example plugin"
  - Solar: "Space theme with violent flaring blue star"
  - Spinfinity: "Simple theme that shows a rotating infinity sign in the center
    of the screen"
  - Spinner: "Simple theme with a loading spinner"

<!-- NOTE: The patches for the below are still in the repo, but aren't currently applied

Plymouth's package that is shipped on Apertis has an additional splash theme
that is called GStreamer and supports displaying the output of a GStreamer
pipeline using the KMS sink gst element.
-->

## Specifying which Plymouth theme to use on boot

There are two ways to specify which theme will be used by Plymouth:

  - Adding `plymouth.theme=${THEME}` as a parameter to the kernel command line
  - Specifying `Theme=${THEME}` in the `/etc/plymouth/plymouthd.conf` Plymouth
    configuration file.

## Plymouth splash theme definition

Each theme is defined on a .plymouth file using the [INI file
format](http://en.wikipedia.org/wiki/INI_file). Following is an example
of the spinner.plymouth file that defines the Spinner splash theme:

    [Plymouth Theme]
    Name=Spinner
    Description=A theme designed by jimmac that features a simple spinner.
    ModuleName=two-step
    
    [two-step]
    ImageDir=/usr/share/plymouth/themes/spinner
    HorizontalAlignment=.5
    VerticalAlignment=.75
    Transition=none
    TransitionDuration=0.0
    BackgroundStartColor=0x202020
    BackgroundEndColor=0x202020

Each Plymouth splash theme uses a ModuleName which is a plug-in that defines
the actual theme behavior and the configuration parameters that can be changed
for a theme. Different themes can use the same module of course and changing
the available parameters for that module.

For example both the Spinner and Glow sample themes use the same two-step
module. Modules are implemented in
[src/plugins/splash](http://cgit.freedesktop.org/plymouth/tree/src/plugins/splash).

## Modifying and adding new Plymouth splash themes

Plymouth is flexible enough regarding with its splash themes and these can be
changed at different levels. If any of the available splash plug-ins fit the
requirement then modifying the boot animation could just be adding a new
`.plymouth` file and modifying the parameters for that module.

Or if none of the available splash themes can be used, a new splash plug-in can
be used to have a more fine grained control of what is displayed on boot.

<!-- NOTE: The patches for the below are still in the repo, but aren't currently applied

For example, Plymouth package on Apertis adds the "apertis" theme.

The GStreamer theme uses a new GStreamer module that had to be developed. So
for that case both a theme and a splash plug-in was added under
`themes/gstreamer` and `src/plugins/splash/gstreamer` respectively.

On the other hand, the ‘apertis’ theme used an already existent "script" module
so only the theme had to be added under `themes/apertis`.
-->

## Testing your theme without reboot and adding debug info

The splash that will be displayed by Plymouth can be tested on X by installing
the plymouth-x11 package:

    $ sudo apt-get install plymouth-x11
    $ sudo plymouthd plymouth.splash=${THEME}"; sudo plymouth show-splash; sleep 5; sudo killall plymouthd

And if you want more debug log, you can add `plymouth:debug` to the kernel
command line.
