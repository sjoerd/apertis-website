+++
title = "Security and Access Control"
weight = 100

aliases = [
    "/old-developer/latest/programming-guide-apparmor.html",
    "/old-developer/v2019/programming-guide-apparmor.html",
    "/old-developer/v2020/programming-guide-apparmor.html",
    "/old-developer/v2021pre/programming-guide-apparmor.html",
    "/old-developer/v2022dev0/programming-guide-apparmor.html",
    "/old-wiki/Guidelines/AppArmor"
]

date = "2016-12-12"
+++

AppArmor is a security layer which enforces access control on the filesystem
resources applications can access, and the permissions they can access them
with. It comprises a kernel module and user space profiles for each
application, which define the resources an application expects to access. For
more information, see the
[AppArmor home page](http://wiki.apparmor.net/index.php/Main_Page). Apertis
uses AppArmor for all applications and services.

# Summary

- [Write AppArmor profiles]( {{< ref "#generating-profiles" >}} ) to be as
  constrained as possible.
- [Validate the profiles]( {{< ref "#validation" >}} ) manually before release,
  and during testing.

# Apparmor Profiles

Apparmor profiles are stored in `/etc/apparmor.d` and are named after the full
path of the executable they confine, with `/` replaced by `.`.

For example, the gpsd executable can be found at `/usr/sbin/gpsd`. This
executable has the apparmor profile `/etc/apparmor.d/usr.sbin.gpsd`.

# Generating Profiles

For application development, the only work which needs to be done for AppArmor
integration is to write and install a profile for the application.

## Development Environment

The OSTree images do not offer a very friendly environment for development, it is best to perform such development using the [apt based images]({{< ref "images.md" >}}). To use the tools mentioned below, the `apparmor-utils` package will need to be installed:

    sudo apt install apparmor-utils

## Creating Profiles

If starting from scratch, you can use `aa-genprof` to create a basic profile.
This will then need to be worked to describe the functionality needed by the
executable.  Profiles should be as constrained as possible, following the
[principle of least privilege](http://en.wikipedia.org/wiki/Principle_of_least_privilege).
See the
[AppArmor website](https://gitlab.com/apparmor/apparmor/-/wikis/Profiles) for
information on writing profiles.

The profiles can be loaded with the following command:

    sudo apparmor_parser -r < /etc/apparmor.d/my.new.profile

Typically this is performed with the profile in `complain` rather than
`enforce` mode. The status of the profiles can be determined by running `sudo
aa-status`.

# Validation

AppArmor profiles can be validated in two ways: at runtime and manually.

Runtime verification is automatic: AppArmor will deny access to files which
violate the profile, emitting a message in the audit logs (`audit.log`), for
example:

    Feb 23 18:54:07 my-host kernel: [   24.610703] type=1400 audit(1393181647.872:15): apparmor="DENIED" operation="open" parent=1 profile="/usr/sbin/ntpd" name="/etc/ldap/ldap.conf" pid=1526 comm="ntpd" requested_mask="r" denied_mask="r" fsuid=0 ouid=0

These can be found with the following commands:

    sudo grep DENIED /var/log/audit/audit.log
    sudo journalctl -b | grep DENIED

Such messages should be investigated, and may result in changes to the
application (to prevent it making such accesses) or to the profile (to allow
them). The
[AppArmor documentation](https://gitlab.com/apparmor/apparmor/-/wikis/QuickProfileLanguage)
covers the
[syntax in more detail](https://gitlab.com/apparmor/apparmor/-/wikis/AppArmor_Core_Policy_Reference).

Manual verification should be performed before each release, manually
inspecting the profile against the list of changes made to the application
since the last release, and checking that each entry is still relevant and
correct, and that no new entries are needed.

Manual and runtime verification are complementary: manual verification ensures
the profile is as small as possible; runtime verification ensures the profile
is as big as it needs to be.

## Installing Profiles

Once the profile is working as required add it to the relevant package
(typically in the `debian/apparmor.d` directory) and
[submit it for review](http://localhost:1313/policies/upstreaming/),


# External links

- [AppArmor home page](http://wiki.apparmor.net/index.php/Main_Page)
- [AppArmor profile wiki page](http://wiki.apparmor.net/index.php/Profiles)
