+++
date = "2020-07-16"
weight = 100

title = "Maintaining Package From Upstream Source"

+++

There are likely to be instances where it is desirable to import the latest version of a piece of software into Apertis directly from it's original authors or maintainers.
This may be as a result of the software in question not being packaged by Apertis' default upstream distribution, Debian, or their being a mismatch between the desired version in the upstream distribution and what is required for a specific goal.

The [Apertis release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ) stipulates that each Apertis release should include the latest mainline kernel LTS release.
Due to Debian's release cadence being approximately half that of Apertis', there are 2 Apertis' releases for each Debian stable release and no guarantees that the kernel's packaged for Debian's current stable or in progress releases will align with Apertis' requirements.
As a result Apertis will need to pull from the mainline kernel LTS tree to satisfy it's requirements.

Such packages will require special attention to assure that they remain up-to-date with any security releases made by the upstream and updated as and when apropriate.

Unless the imported package can be used as-is without any modification, there will be a [patch series]( {{< ref "buildingpatchingandmaintainingtheapertiskernel.md#packaging" >}} ) that potentially needs tweaking to apply after the update.
The workflow set out below takes this into consideration.

Below we use the process of updating the Linux kernel for `v2020` as an example, where we track the `linux-5.4.y` stable branch:

- Using the GitLab web UI, check to ensure that the relevant update branch exists, in the case of the Apertis `v2020` release, kernel updates should be made on the `apertis/v2020-security` branch. Create the required branch if it doesn't exist.
- Clone the existing package from Apertis GitLab and checkout the relevant branch:

      $ git clone -b apertis/v2020-security git@gitlab.apertis.org:pkg/target/linux.git linux-apertis

- Separately clone the Linux kernel LTS repository and checkout the relvant stable branch:

      $ git clone -b stable-linux-5.4.y git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git linux-stable

- Create a `gbp pq` patch branch and switch back to normal branch. The patch branch will be needed when rebasing the Debian patches:

      $ cd linux-apertis
      $ gbp pq import
      $ gbp pq switch

- Determine the latest stable release and derive the Apertis version to use. To ensure collisions don't occur with any future Debian releases, we will mark the release as the zeroth "Debian" release (`-0`) and first Apertis pre-release, as applied by Collabora (`~co1`):

      $ RELEASE=`git -C ../linux-stable describe`
      $ RELEASE="${RELEASE#v}-0~co1"

- Add a changelog entry for newest `linux-5.4.y` kernel release (this is needed to get `genorig.py` to use the right release):

      $ DEBEMAIL="User Name <user@example.com>" dch -v "$RELEASE" ""
      $ git add -f debian/changelog
      $ git commit -s -m "Add changelog entry for update to $RELEASE"

- Run `debian/bin/genorig.py` to generate debianised kernel source tarball:

      $ debian/bin/genorig.py ../linux-stable

- Import the updated source tarball:

      $ gbp import-orig --upstream-branch=upstream/linux-5.4.y --debian-branch=apertis/v2020-security <path to orig tarball>

- Rebase the Debian patches on the new kernel version:

      $ gbp pq rebase

  This may require some manual intervention following the normal git rebasing process.

- Export updated patch series:

      $ gbp pq export

- Tweak `debian/patches/series` file so that it retains the comments that `gbp pq` wants to drop. If no patches have been dropped during rebase then:

      $ git checkout debian/patches/series
      $ git add -f debian/patches
      $ git commit -s -m "Update the debian patches for $RELEASE update"

- Update changelog to released state (set distribution to apertis), document any patches that have been dropped as a result of the update and create new commit:

      $ dch -D apertis
      $ git add -f debian/changelog
      $ git commit -s -m "Release linux version $RELEASE"

- Run `dpkg-buildpackage` to generate the debian packaging, this is needed to use the tools to update the [pristine-lfs branch]( {{< ref "gitlab-based_packaging_workflow.md" >}} ):

      $ mkdir ../build
      $ gbp buildpackage --git-ignore-new \
          --git-debian-branch=apertis/v2020-security \
          --git-prebuild='debian/rules debian/control || true' \
          --git-export-dir='../build' \
          -us -S

- Import the generated kernel tarball into pristine-lfs branch with `import-tarballs`:

      $ git clone git@gitlab.apertis.org:infrastructure/packaging-tools.git ../packaging-tools
      $ ../packaging-tools/import-tarballs ../build/linux_5.4.51-0~co1.dsc
      $ git push origin pristine-lfs

- Push kernel update to a branch to be reviewed:

      $ git push origin apertis/v2020-security:wip/user/kernel-update
