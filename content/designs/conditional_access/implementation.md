+++
date = "2015-12-07"
weight = 100

title = "Conditional Access Implementation"

aliases = [
    "/old-wiki/Conditional_Access/Implementation"
]
+++

{{% notice warning %}}
This concept is an old work in progress.
{{% /notice %}}

{{% notice warning %}}
These notes on proposed implementations of
[Conditional Access]( {{< ref "/designs/conditional_access/_index.md" >}} )
are not up-to-date with the current view of the requirements.
{{% /notice %}}

## Proposed implementation

### Security model

The properties we are protecting are:

  - confidentiality of the contents of app-bundles
  - integrity of the information that the app manager uses to decide
    whether an app-bundle can be launched

The attacker is a user of Apertis devices who aims to read the
DRM-protected content (such as music) contained in the app-bundle, or to
induce the Apertis system to allow them to launch the app-bundle when it
should not.

This document does not attempt to address the integrity of app-bundles.
We recommend addressing this separately: for example, the [Access
token signing key](#access-token-signing-key) must be kept
online, whereas it might be possible to sign app-bundles for integrity
with a key that is kept offline, which would reduce its exposure.

### Cryptographic keys

**Unresolved:** We do not currently have concrete recommendations for
cryptographic algorithms or key sizes. The recommendations here should
take into account that the expected lifetime of a car is long, and
algorithms/key sizes currently believed to be adequate to resist any
realistic attacker will not necessarily remain adequate indefinitely.

#### Access token signing key

The *access token signing key* is an asymmetric key-pair (public and
private key) used for digital signatures (for example RSA or ECC). The
private half must be available to the app-store, and nowhere else; it
must be kept constantly available (i.e. online) so that it can produce
new access tokens on-demand. The public half must be available on all
Apertis devices.

A signature from this key signifies that the [Access
token](#access-token) was issued by the app-store; Apertis
devices that are configured to trust this key will accept this signature
as evidence that they may use the application named in the access token,
subject to the other parameters in the access token.

For key agility, we recommend maintaining multiple access token signing
keys that are all equally trusted by Apertis devices, for example with
all but one of the private keys kept offline; for algorithm agility, it
would be possible for those multiple keys to use different digital
signature algorithms. These access token signing keys could have an
expiry date to limit the impact of a compromise.

*Impact of compromise:* If the private half of this key becomes
available to an attacker, then that attacker can give arbitrary devices
permission to launch any application (in other words, conditional access
is defeated) until those devices consider that key to have expired.

#### Device key

The requirement that a user cannot extract content from a downloaded
app-bundle, but the Apertis device can, implies that each Apertis device
must be provisioned with some secret not available to the user (the
*device key*), which is used to decrypt the app-bundle.

We recommend that the manufacturer generates a unique asymmetric
key-pair consisting of a public and private key, for an asymmetric
crypto-system such as RSA or ECC, and uses that as the device key. Each
device needs to be provisioned with a unique [Device
ID](#device-id) (serial number) in any case, so no
additional effort is needed to provision the devices with unique device
keys at the same time.

The public half of the key-pair must be stored by the app-store, along
with the corresponding device ID.

The private half of the key-pair must be stored on the device, and
nowhere else. The reason for this recommendation is also the reason why
we use asymmetric cryptography here. If private or symmetric keys were
stored by the app-store, then the app-store would be a significant
target for attack: any attacker able to obtain private device keys would
be able to decrypt any app-bundle intended for any device.

We recommend that no two Apertis devices share a device key. In theory
it would be possible to share a device key between a class of identical
devices, but again, this would make an attack more attractive by
increasing the potential gain by an attacker: obtaining one device key
would allow decryption of any app-bundle intended for any device that
shared that key.

*Impact of compromise:* If the private half of the device key becomes
available to an attacker, then that attacker can obtain the [Ephemeral
encryption key](#ephemeral-encryption-key) for any encrypted
app-bundle that was validly available for installation on the device
with that key, and therefore can read the contents of that app-bundle.

#### Session key

Each time the content of a restricted app-bundle is made available for
download for one or more devices, the app store generates a new
cryptographically random symmetric encryption key, the *session key*.
This is then encrypted using the public halves of one or more device
keys, such that it can only be decrypted by the devices holding the
private halves of those keys: this is the "[hybrid
cryptosystem](https://en.wikipedia.org/wiki/Hybrid_cryptosystem)" design
commonly used to encrypt a message for multiple recipients, for example
in OpenPGP and PKCS\#7.

*Impact of compromise:* If the session key becomes available to an
attacker, then that attacker can decrypt that particular copy of a
restricted app-bundle and read its contents.

### Implementation of download and installation

We recommend representing a downloaded app-bundle as follows:

  - generate a new [Session key](#session-key)
  - use the session key to encrypt a clear-text version of the
    app-bundle (assumed to already contain whatever ISV and/or store
    signatures are needed for [App-bundle
    integrity](#app-bundle-integrity), which is out of scope
    here), resulting in an *encrypted bundle*
  - for each authorized device, sign a file containing at least the
    following, resulting in a *signed access token*:
      - the bundle ID of the bundle to be licensed
      - **Unresolved:** Should this also include a cryptographic hash of
        the (encrypted or clear-text?) app-bundle, to bind the access
        token to a specific app-bundle implementation? This would make
        the access token version-specific, such that a new app-bundle
        version would always need to come with a new access token.
      - timestamp (seconds since a fixed "epoch" time, in UTC) at which
        this access token was generated
      - timestamp (seconds since a fixed "epoch" time, in UTC) at which
        this access token will expire, or an indication that it will
        never expire
      - the entity holding the license (store account ID, vehicle ID or
        device ID)
      - if the entity holding the license is a store account ID or a
        vehicle ID: the device ID for which this particular access token
        will be encrypted
  - for each access token, encrypt the signed access token and the
    [Session key](#session-key), using the corresponding
    *device key*, resulting in an *encrypted access token*
  - send the *encrypted bundle* and one or more *encrypted access
    tokens* to the user, either as separate files, a file for the
    encrypted bundle and a second file containing all encrypted access
    tokens, or a single file containing everything

**Unresolved:** In the current design we are encrypting the access
tokens, which has the effect of ["binding" the signature to the
encryption](http://world.std.com/~dtd/sign_encrypt/sign_encrypt7.html).
Are we gaining any desired security properties from doing so, or would
it be sufficient to send an encrypted session key along with a signed
but unencrypted access token? Does an attacker gain anything from the
ability to read access tokens?

For an [App-bundle licensed to a store
account](#app-bundle-licensed-to-a-store-account), if a
user's store account is associated with multiple Apertis devices, and
the user asks to download an app-bundle for installation via USB
storage, then they should receive an encrypted access token for each of
their devices, each containing the same value of the session key, so
that they can all decrypt the same encrypted bundle. The session key may
either be generated once for each revision of a bundle, or once for each
request to download files from the store. A different session key should
be used whenever the encrypted content differs, so that compromise of a
session key would not allow decryption of unrelated encrypted content.

Similarly, for an [App-bundle licensed to a
device](#app-bundle-licensed-to-a-device) or an
[App-bundle licensed to a
vehicle](#app-bundle-licensed-to-a-vehicle), any user whose
store account is associated with the relevant Apertis device should be
able to download an encrypted access token for that device, or for the
device installed in that vehicle. Only the relevant device will be able
to decrypt the resulting encrypted access token.

The value of the session key should not be stored by the Apertis device,
and can be discarded after installation. The *signed access token*,
including its signature, should be kept (see [Running an
application](#running-an-application)).

For [unrestricted app-bundles](#unrestricted-app-bundle),
the generation of the session key and all encryption steps may be
skipped if desired. For symmetry, we suggest that if unrestricted
bundles are unencrypted, they should still come with an unencrypted
signed access token, with:

  - the bundle ID of the bundle to be licensed
  - the timestamp at which the access token was generated (it can be a
    single access token for everyone)
  - an indication that this access token is valid for everyone
  - an indication that it will never expire
  - no store account IDs, vehicle IDs or device IDs

but without including a session key.

Alternatively, unrestricted bundles could be encrypted with a session
key in the same way as restricted app-bundles, and the app store could
simply provide access tokens for all the devices associated with any
store account at no cost to the user.

### Implementation of running an application

The application framework should store signed access tokens in a
directory that can be written by the application installer, and read by
the application launcher (Canterbury). For example, they could be placed
in directories named `/var/Applications/$bundle_id/tokens/` where
`$bundle_id` is the bundle identifier. The [Session
key](#session-key) does not need to be stored and should be
discarded after installation.

All bundles' tokens directories must be readable by all users'
application launcher processes. We suggest that they should be owned by
`root` with the standard `rwXr-Xr-X` permissions (0755 for directories,
0644 for files containing tokens). We suggest that the signed tokens
should be stored unencrypted, for simplicity: nothing is lost by not
encrypting them, because any process that does not have a legitimate
need to read them will be prevented from doing so by AppArmor profiles.
If they were encrypted, then any process that has a legitimate need to
read them would also need to be allowed to read a key that can decrypt
them, for example the session key, which would merely move the problem
from "protect the bundle contents with AppArmor" to "protect the
decryption key with AppArmor"; AppArmor is still part of the TCB either
way.

To allow an application bundle to remind a user that its time limit will
soon expire, we suggest that the tokens should remain unencrypted, and
each bundle's AppArmor permissions should allow it to read its own
tokens directory. This would also allow for [Per-feature conditional
access](#per-feature-conditional-access) if desired.

The application manager should not allow launching an application unless
its access tokens are consistent with the vehicle ID, device ID, etc.
that are current at the time of launching.

The version of the signed access token that is stored should include the
signature, so that the application manager can verify that the timestamp
has not been modified.

**Unresolved:** Is there any realistic threat model where the attacker
could modify the timestamp but could not modify the code that will
validate it? (But the cost of keeping the signature is negligible, so we
should keep it anyway, even if there's no point.)

### Implementation of time-limited access

The timestamps in a signed access token are timestamps, not relative
times (so for example a token might represent "you may use
com.example.Game from 2016-01-01 00:00:00+0000 to 2016-01-31
00:00:00+0000", but never "you may use com.example.Game from now until
now + 30 days"). This prevents replay attacks: for example, if a token
represented "now to now + 30 days", a user could place it on a read-only
USB storage medium and present it to the Apertis device repeatedly,
potentially receiving a different intepretation for the meaning of "now"
every time.

To have a precise definition for whether a time has expired, we
recommend that the timestamps in the signed access token are defined to
be UTC (not influenced by the time zone of the server, user, app-store
curator or ISV). We also recommend that the real-time clock is also
represented as UTC internally, and conversion from UTC to the user's
configured time zone is only done when the time is displayed (this is
how real-time clocks in Linux normally work, and is mentioned here for
completeness).

When "refreshing" an installation (for example to renew the lifetime of
a bundle, convert a trial version into a paid version, or ensure
continued access to a subscription-based bundle), we recommend that the
app store generates a new *signed token* with updated timing information
and encrypts it using the device key. If the store account is associated
with multiple devices and the user is downloading for installation via
USB storage, the app store should generate a signed token for each
device, and provide them in a single file or several adjacent files.
There is no need to include a session key unless the user is also
downloading an encrypted app bundle; if the.

**Unresolved:** As with the [Implementation of download and
installation](#implementation-of-download-and-installation),
is there any security benefit to the encryption here? If there is not,
then we do not necessarily need to encrypt to the device key at all.

If an old access token is superseded by a new access token with a later
expiry time, the old access token should be deleted after the new token
has been validated.

As previously noted, we recommend that a [Trial
version](#trial-version) is simply implemented as a
[Time-limited app-bundle](#time-limited-app-bundle), with
the app store's business logic set up such that it will allow one
short-term access token per store account, device or vehicle to be
"purchased" at no cost; this requires a small amount of additional logic
in the app store, but no new logic on the Apertis device that is not
already required for other scenarios.

A local attacker (the user) could alter the real-time clock to try to
extend a time-limited bundle's validity. For example, suppose an access
token is valid from 2016-01-01 00:00:00+0000 to 2016-01-31
00:00:00+0000; on 2016-02-01, the attacker could reset the clock to
2016-01-01 00:00:00+0000 to get another 30 days of validity for an
application that has already expired. Similarly, on 2016-01-16, the
attacker could reset the clock to 2016-01-01 to get another 15 days of
validity for an application that has not expired yet.

The application manager should not allow launching an application unless
its access tokens are consistent with the time that is current at the
time of launching. However, we are not assuming that the real-time clock
is trustworthy, and time-limited access with an untrusted clock is a
difficult problem. The worst-case scenario under our
[Assumptions](#assumptions) is as follows:

  - The real-time clock is entirely attacker-controlled
  - The real-time clock can be altered while the CE domain is switched
    off, either via the automotive domain or by accessing the hardware
  - The attacker resets the real-time clock to the same value before
    every boot
  - There is no trusted time source such as GPS, cellular networks or
    anything Internet-connected, and we receive app-bundles via USB
    storage

In this worst-case scenario, the best we can do is to count how long the
CE domain has been running since the app-bundle was received, and make
it expire after 30 days of uptime. With a trusted time source, we can do
somewhat better than this.

We suggest re-examining the assumptions to determine whether any
stronger assumptions can be made (for example, a trustworthy real-time
clock). If they cannot, we suggest the rather elaborate approach
outlined in the next section.

#### Partially preserving time-limited access in the face of an untrusted time-source

  - Maintain an *uptime counter* as follows:
      - During boot, load the *stored uptime counter* from storage. If
        none has been stored yet, its value is 0.
      - The uptime counter is the stored uptime that was loaded during
        boot, plus the monotonic timestamp (seconds since boot) that is
        provided by Linux. It represents the total time the device has
        been running.
  - During clean shutdown, and periodically during runtime (so that it
    can be used after an unclean shutdown), save the uptime counter as
    the stored uptime counter. This ensures that the uptime counter
    never decreases, and is always less than or equal to the device's
    total runtime. (For example, if the device is shut down uncleanly 1
    minute after updating the stored uptime counter, then the uptime
    counter will be 1 minute behind what it should be from then on.)
  - Also maintain a lower bound for the real-time clock, as follows:
      - We will sometimes receive a *trusted lower bound* for the real
        time, such as a signed access token from the app-store that is
        declared to have been generated at a particular time. Keep a
        record of the newest *trusted lower bound* that we have ever
        received, and the *uptime counter* at the time we received it.
        "Newest" here means the most recent time declared by the
        app-store, not the time from the access token that we processed
        most recently; this is to avoid our time estimate going
        backwards if a user downloads multiple app-bundles and installs
        them in a different order.
      - The *lower bound* is the newest trusted lower bound, plus the
        uptime counter now, minus the uptime counter when we received
        the newest trusted lower bound.
      - For example, suppose the user downloads app A to USB storage on
        2016-01-11, downloads app B to USB storage on 2016-01-12,
        installs app B when our uptime counter was 100 days, and
        installs app A when our uptime counter was 103 days. Suppose our
        uptime counter is now 105 days. The *trusted lower bound* is
        2016-02-12, received 5 days of uptime ago, so the resulting
        *lower bound* is 2016-01-17. If app C was meant to expire on
        2016-01-15, we now know that it has indeed expired.

We suggest that the application manager should treat applications as
expired unless their expiry dates are newer than both the real-time
clock and the *lower bound*. Effectively, the lower bound puts a limit
on how much the attacker can stretch the lifetime of an app-bundle by
resetting the clock.

The *trusted lower bound* can also be used as input into TLS certificate
validity decisions, for example in HTTPS: if a TLS certificate's expiry
date is prior to the trusted lower bound, then we know that it has
expired, even if the real-time clock says otherwise.

When deciding whether particular time-sources can be used as a *trusted
lower bound*, we must bear in mind that an attacker able to influence
these time-sources can use them as a long-term denial of service attack.
For example, if an attacker can spoof GPS or GSM signals and tell the
Apertis device that the date is sometime in 2020, and the Apertis device
treats this as a trusted lower-bound, then that device will not be able
to use any app-bundle that is meant to expire earlier than 2020. Once
carried out, this attack cannot be reversed without either real-time
Internet access, or building in a vulnerability to replay attacks. As
such, we recommend that only strongly-authenticated time-sources are
used for the *trusted lower bound*.

If the real-time clock becomes inconsistent with the *lower bound*, the
requirement that accidental incorrect setting of the real-time clock can
be "forgiven" by resetting the clock means that we cannot force the
app-bundles to expire. There is a trade-off between usability by
non-malicious users who might set the clock incorrectly, and the
inability of malicious users to bypass time limits. One possibility to
avoid this issue would be to ensure that the settings HMI makes it
impossible to set the real-time clock earlier than the lower bound.

Depending on the approach chosen for that trade-off, another possibility
would be to flag implausible real-time clock values (earlier than the
lower bound) as a possible attack, disable all app-bundles that have a
limited expiry time, and require the user to re-activate those
app-bundles by downloading new access tokens; if desired, the app-store
could flag store accounts that have done this for closer examination.
However, if this happens to a user who is not attempting to attack the
system but has merely set the clock incorrectly, this is likely to lead
to user resentment and bad PR. We recommend considering this trade-off
carefully.

### Implementation of developer access

Some of the states described in [Developer
access](#developer-access) result in Before the device is
put in a state where it can no longer be trusted to enforce conditional
access, all application bundles that are subject to conditional access
must be deleted. We recommend that this is presented as a whole-device
"factory reset" (data wipe), similar to the reset that is implied by
[unlocking the bootloader of Google's Nexus reference devices for
Android](http://www.androidcentral.com/how-unlock-galaxy-nexus-bootloader).
This satisfies the requirement that app-bundle contents not be exposed.

After resetting, the device key will have been deleted. This satisfies
the requirement that app-bundles with conditional access are not
installable: the device will be unable to decrypt the files that
encapsulate those app-bundles, because it no longer has its provisioned
device key.

We recommend that SDK/development images, and most target images for the
open-source Apertis platform, are shipped in an "unlocked" state where
conditional access is unavailable but a reset/wipe is not needed. We
recommend additionally producing one target image for the open-source
Apertis platform that is provisioned with a well-known device key, or
instructions for doing so, so that conditional access can be tested
against that device key.

If a developer device needs to be put back into a "production" mode, for
example because it is returned to a trusted manufacturer or dealer for
repair, the trusted party could wipe its contents, install a trusted
version of the OS, and re-provision it with a newly-generated device key
that is also sent to the app-store. This would return the device to a
state where it can be trusted to enforce conditional access.

If the device key is situated in [sealed
storage](https://en.wikipedia.org/wiki/Trusted_Computing#Sealed_storage),
for example provided by a TPM (Trusted Platform Module), the system
would not necessarily be required to delete it during the switch to
developer mode; it could merely make the device key inaccessible until
the OS is replaced by a trusted one. However, it would still be
necessary to delete app-bundles during the switch to developer mode.

This is somewhat linked to
[Verified boot]( {{< ref "system-updates-and-rollback.md#verified-boot" >}} )
("secure boot"), which can verify whether the operating system being
booted is a trusted one or not.

### Implementation of themes

For themes with time-limited access, the application manager should
unmap the theme from the themes directory (delete the symbolic link or
unmount the bind-mount, as applicable) when the use time expires. It
should also change the theme setting, if the expired theme was the
current one; this theme change should be propagated to applications in
the same way as any other theme change. For simplicity, we recommend
resetting the theme to the factory default when this is done.

If a reset to the most recently used non-expired theme is required, we
recommend that the application manager and the configuration application
cooperate to maintain a list of recently-used themes, with some
arbitrary cutoff (perhaps a maximum of 10 themes in the history). In the
unlikely situation that all of the themes in the history have expired or
been removed, the factory default should be assumed.

The same list of recently-used themes could be used when a current theme
is uninstalled, if desired.

The configuration application should not allow empty/deleted themes to
be selected.
