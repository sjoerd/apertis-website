+++
date = "2019-02-28"
weight = 100

title = "Quality Assurance"

aliases = [
    "/old-wiki/QA"
]
+++

This is the page for the Quality Assurance team. It includes links for
QA services, documentation, tests, among with any information related to
the different QA processes in the project.

  - [Test cases](https://qa.apertis.org/)

## Documentation

  - [Test Cases Guidelines]( {{< ref "/qa/test_cases_guidelines.md" >}} )
  - [LQA Tool]( {{< ref "/qa/lqa.md" >}} )
  - [Personal LAVA Tests]( {{< ref "/qa/personal_lava_tests.md" >}} )

## Weekly tests reports

The images are tested weekly using both manual and automated tests and a report
is generated on the [QA report application](https://lavaphabbridge.apertis.org/).

## Services

[LAVA (Linaro Automated Validation Architecture)](https://lava.collabora.co.uk)

  - [LAVA Documentation](https://lava.collabora.co.uk/static/docs/v2/index.html)

## Tools

Tools used for QA tasks and infrastructure.

  - [lqa](https://gitlab.collabora.com/collabora/lqa): It submits the
    automated tests jobs to LAVA. It also offers a LAVA API that can be
    used by other scripts.
  - [phab-handler](https://git.collabora.com/cgit/singularity/tools/phab-handler.git/):
    It updates the Phabricator tasks status by the B\&I infrastructure.
  - [phab-tasks](https://git.collabora.com/cgit/singularity/tools/phab-tasks.git):
    Tool that helps checking Phabricator tasks status. It is mainly used
    to check for new bugs during the weekly testing rounds and helps to
    generate the email report.
  - [lwr](https://gitlab.collabora.com/collabora/lwr): The LAVA weekly
    round tool is used to fetch the automated tests results and generate
    the wiki report page for the weekly testing round.
