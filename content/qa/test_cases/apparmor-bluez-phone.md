+++
date = "2016-02-29"
weight = 100

title = "apparmor-bluez-phone"

aliases = [
    "/old-wiki/QA/Test_Cases/apparmor-bluez-phone"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
