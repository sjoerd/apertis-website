+++
date = "2020-01-15"
weight = 100

title = "Home Page"

aliases = [
    "/old-wiki/Main_Page",
    "/old-wiki",
    "/old-designs",
    "/old-designs/latest",
    "/old-designs/v2019",
    "/old-designs/v2020",
    "/old-designs/v2021dev3",
    "/old-developer",
    "/old-developer/latest",
    "/old-developer/v2019",
    "/old-developer/v2020",
    "/old-developer/v2021pre",
    "/old-developer/v2022dev0",
]
+++

Apertis is a versatile
[open source infrastructure]({{< ref "platform-guide.md" >}}) tailored to the
automotive needs and fit for a wide variety of electronic devices.  Security
and modularity are two of its primary strengths. Apertis provides a
feature-rich framework for add-on software and resilient upgrade capabilities.
Beyond an operating system, it offers new APIs, tools and cloud services.

# Production Friendly

Apertis aims to accomplish the following goals:

* Ensure that all the software shipped in Apertis is open source or at least freely distributable, so that downstreams are entitled to use, modify and redistribute work derived from our deliverables.
* Ensure that Apertis images targeting devices (such as target and minimal), are not subject to licensing constraints that may conflict with the regulatory requirements of some intended use cases.

Apertis is primarily licensed under the Mozilla Public License Version 2.0, with images and documentation licensed under CC BY-SA 4.0.

For more information see [Open source License expectations]( {{< ref "license-expectations.md" >}} ).

Apertis undergoes regular [automated and manual testing](https://qa.apertis.org) on the [reference hardware]( {{< ref "/reference_hardware/_index.md" >}} ) with [publicly available results](https://lavaphabbridge.apertis.org).


## Further Links

* [Policy documentation]( {{< ref "policies" >}} )
* [Quality assurance]( {{< ref "/qa/_index.md" >}} )
* [Test results](https://lavaphabbridge.apertis.org)


# Frequent Releases

The overall goal is for Apertis to do a yearly product release. A product release is intended to both be based on the most recent mainline kernel LTS release and the current Debian stable release. Since Debian releases roughly once every two years, that means that there will typically be two Apertis product releases based on a single Debian stable release. With Linux doing an LTS release on a yearly basis, each Apertis product release will be based on a different (and then current) Linux kernel release.

The standard support period for each Apertis product release is 7 quarters. In other words from the initial release at the end of Q1 until the end of the next year.

For more information, see [Release flow and product lines]( {{< ref "release-flow.md" >}} ).


## Further Links

* Current stable release: [v2020.3]( {{< ref "/release/v2020.3/releasenotes.md" >}} )
* Current old stable release: [v2019.5]( {{< ref "/release/v2019.5/releasenotes.md" >}} )
* Current preview release: [v2021pre]( {{< ref "/release/v2021pre/releasenotes.md" >}} )
* Current development release: [v2022dev0]( {{< ref "/release/v2022dev0/releasenotes.md" >}} )
* [Further releases]( {{< ref "releases.md" >}} )


# Developer Ready

Apertis provides a number of [pre-built images]({{< ref "download.md" >}}) for
Intel (64-bit) and ARM (32-bit and 64-bit) for evaluation on one of the Apertis
[reference hardware]( {{< ref "/reference_hardware/_index.md" >}} ) platforms.
Additionally Apertis provides an SDK image to simplify development for the
platform.

![The Apertis SD](/images/vm-sdk.png)


## Further Links

* [Download the current images]( {{< ref "download.md" >}} )
* [Source code](https://gitlab.apertis.org)
* [Architectural documentation]( {{< ref "architecture" >}}) 
* [Development guides]( {{< ref "guides" >}} )
* [Design documents]( {{< ref "designs" >}} )
* [Concept designs]( {{< ref "concepts" >}} )
