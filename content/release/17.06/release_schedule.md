+++
date = "2017-04-05"
weight = 100

title = "17.06 Release schedule"

aliases = [
    "/old-wiki/17.06/Release_schedule"
]
+++

The 17.06 preview release cycle starts in March 2017. You can find an
outline of the planned development in the
[Roadmap]( {{< ref "/roadmap.md" >}} ).

<table>
<thead>
<tr class="header">
<th><p>Milestone</p></th>
<th><p>Date</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Start of release cycle</p></td>
<td><p>2017-03-25</p></td>
</tr>
<tr class="even">
<td><p>Feature proposal freeze: end of feature proposal and review period<br />
New features must be proposed, reviewed and accepted by this date. Any features which are not accepted by this date will need to be proposed for a future release.</p></td>
<td><p>2017-05-16</p></td>
</tr>
<tr class="odd">
<td><p>Soft code freeze: end of feature development for this release<br />
From this date onwards, only bug fixes will be accepted into the image.</p></td>
<td><p>2017-06-09</p></td>
</tr>
<tr class="even">
<td><p>Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date</p></td>
<td><p>2017-06-19 EOD<br />
(image <code>20170620.*</code>)</p></td>
</tr>
<tr class="odd">
<td><p>RC1 testing<br />
No new code is added to the image unless a critical bug or security issue is found.</p></td>
<td><p>2017-06-21/2017-06-22</p></td>
</tr>
<tr class="even">
<td><p>17.06 release</p></td>
<td><p>2017-06-23</p></td>
</tr>
</tbody>
</table>

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
