+++
date = "2018-09-27"
weight = 100

title = "18.06 ReleaseNotes"

aliases = [
    "/old-wiki/18.06/ReleaseNotes"
]
+++

# Apertis 18.06 Release

**18.06** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both the 32bit ARMv7 and 64-bit ARMv8
versions using the hardfloat ABI) and Intel x86-64 (64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customizations. Test results of the 18.06 release
are available in the [18.06 test report]().

### Release downloads

| [Apertis 18.06 images](https://images.apertis.org/release/18.06/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |
| ARM 32-bit (U-Boot)                                               |
| ARM 32-bit (MX6q Sabrelite)                                       |
| ARM 64-bit (U-Boot)                                               |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 18.06 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 18.06 target helper-libs development sdk hmi`

## New features

### Official supported images for i.MX6 Sabrelite boards

The [i.MX6
Sabrelite]( {{< ref "/reference_hardware/imx6q_sabrelite_setup.md" >}} ) board
is now officially supported by the main image set with the new
`mx6qsabrelite` image variant.

The headless minimal images are fully supported, while images with the
full grahics stack are still in the works.

This means that anybody can deploy the publicly available
`mx6qsabrelite` system images on i.MX6 Sabrelite and start experimenting
with Apertis on their hardware.

### Linux kernel v4.14 available on all the supported platforms

The 4.14 LTS version of the mainline Linux kernel is now used by every
supported platform. Previously different releases were used on each
platform, ranging from 3.14 to 4.4 and 4.9, all build from different
source packages.

Apertis now offers support of all the supported platforms using kernels
build from a single, common source packages, providing an updated and
supported baseline and simplifying maintainance and development of new
features.

### SDK for platform development

The focus for the Apertis SDK so far has been centered on application
development but from this release we started to make it more useful for
platform developers that want to hack to the base Apertis system. In
this release new packages for the SDK have been made available,
configuration tweaks have been applied and several little nuisances have
been addressed.

### Code hosting on GitLab

All the Apertis code repositories have been migrated to the new [GitLab
instance hosted by the Apertis team](https://gitlab.apertis.org/public).

GitLab brings Apertis many benefits over the previous setup:

  - better web UI
  - site-wide source code search
  - the availability of a merge-request based review workflow
  - finer grained access controls
  - built-in continuous automation

Phabricator is still used for task tracking and for patch review. A
GitLab merge-request based review workflow to replace the latter is
being defined.

The [CGit code hosting](https://git.apertis.org/) service has been
obsoleted and is now read-only. Redirects have been put in place to
route requests from the old service to tne new one.

### Closed the CI loop by automatically opening issues on automated test failures

A way to automatically file new bugs and update them when a test is
failing has been designed and its implementation is being finalized.

A new service gets tests results out of LAVA, processes them and then
connects to the Phabricator APIs to open new task or update existing
ones.

Managers can now rely on the tasks on Phabricator to check the health of
the testsuite and developers get timely updates about the latest status
of failing tests.

## Quality Assurance

### Testcase prioritization

A priority level has been assigned to each
[testcase]( {{< ref "/qa/test_cases/_index.md" >}} ) in order to better schedule their
execution, to clearly identify release blockers and to prioritize
maintainance and improvements.

### Testcases ported to run on the OSTree images

More testcases have been ported to avoid dependencies in a way that they
can be run without installing new packages on the base image.

This has two benefits:

  - makes testing more valuable by not perturbing the actual base image
  - allows those tests to be executed on OSTree images

### Test ADE as part of the continuous test rounds

Tests exercising cross-building and remote debugging using the ADE
(Apertis Development Environment) tool are now part of the [weekly test
reports]().

## Build and integration

### Image recipes cleanups

The [Apertis image
recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes)
have seen several cleanups and improvements in order to make them
simpler, faster, more robust, and easier to maintain and customize.
Further improvements are planned and being worked on.

## Design and documentation

### All documents available in PDF format

Line-numbered PDF versions of the [design
documents](https://designs.apertis.org/) are available for download in
order to provide a better offline reading experience.

To get them, replace the `.html` extension in the URL with `.pdf`, for
instance
[system-updates-and-rollback.html]( {{< ref "system-updates-and-rollback.md" >}} )
becomes
[system-updates-and-rollback.pdf](https://designs.apertis.org/latest/system-updates-and-rollback.pdf).

### Improving storage and handling of the definitions and results of testcases

A new set of concept documents has been worked on to address some
shortcomings in the current infrastructure about the best way to store
testcase definitions, to handle test results and to process those
results to provide reliable reporting.

### Improvement to the *System updates and rollback* document

A list of use-cases has been added to the [System updates and
rollback]( {{< ref "system-updates-and-rollback.md" >}} )
design document in order to guide the evaluation of features and
approaches.

Several other improvements and clarifications have been applied to the
document based on the feedback gathered.

### Security aspects of OSTree

The [updates and rollback design
document](https://designs.apertis.org/latest/system-updates-and-rollback.html%7CSystem)
has been updated to cover the security aspects of OSTree, describing how
updates are verified and how it relates to other technologies that can
be used in verified boot solutions.

### Examples of key OSTree commands for Apertis images

A few OSTree commands have been collected in
[Docs/OSTree]( {{< ref "/guides/ostree.md" >}} ) to provide a reference of
important operations for developers:

  - updating the system
  - list locally available deployments
  - rolling back to previous releases
  - enabling temporary read-write access to the system

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Development images

<dl>

<dt>

The `development` image type is deprecated in favour of extending the
`target` image type

<dd>

From the next release Apertis will no longer ship `development` images,
covering the use-cases addressed by them by adding the needed packages
to the `target` image type.

</dl>

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit nor any accompanying libraries, as the underlying Clutter
library has been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester-full` and `canterbury-full` are deprecated

<dd>

From the next release Canterbury and Ribchester will only offer the APIs
provided by the respective `-core` and the additional APIs offered by
the `-full` packages will be dropped

</dl>

### Breaks

<dl>

<dt>

SSH addresses for Git repositories need to be manually updated

<dd>

With the move to GitLab the location of the Apertis Git repositories has
changed: HTTP(S) redirects have been put in place to transparently route
requests, but no similar mechanism is available for SSH, so SSH remotes
will need to be updated manually

<dt>

`xterm` is no longer available on the SDK images, replaced by
`xfce4-terminal`

<dd>

The lack of support for "modern" features like supports copy-and-paste
in <code>xterm<code> confused some users and it has been replaced by
`xfce4-terminal`

</dl>

## Infrastructure

### Apertis infrastructure tools

The Apertis Debian Stretch tools repositoriy provides packages for
`git-phab`, `lqa`, Debos and deps, LAVA v2, OBS 2.7, `ostree-push` and
`jenkins-job-builder`:

` $ deb `<https://repositories.apertis.org/debian/>` stretch tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (2)

  - Tiny container can't be started in non-privileged mode with AppArmor

  - Unable to boot an image for the second time on imx6q-sabrelite board
    for LAVA setup

### Normal (58)

  - connman shows incorrect Powered status for the bluetooth technology

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - cgroups-resource-control: blkio-weights tests failed

  - libsoup-unit: ssl-test failed for ARM

  - Fix folks EDS tests to not be racy

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - gupnp-services: test service failed

  - Crash when initialising egl on ARM target

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Test apps are failing in Liblightwood with the use of GTest

  - apparmor-tracker: underlying_tests failed

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - apparmor-folks: unable to link contacts to test unlinking

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - folks: random tests fail

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - Observing multiple service instances in all 17.06 SDK images

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - Debos: allow to use partition number for 'raw' action

  - Debos: mountpoints which are mounted only in build time

  - Debos: option for kernel command parameters for 'filesystem-deplay'
    action

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - Volume Control application hangs on opening it

  - gupnp-services: browsing and introspection tests fail

  - sqlite: Many tests fail for target amd64, armhf and sdk

  - dbus-installed-tests: service failed because a timeout was exceeded

  - boot-no-crashes: ANOM_ABEND found in journalctl logs

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - ade process fails to start gdbserver if a first try has failed

  - SDK hangs when trying to execute bluez-hfp testcase

  - OSTree testsuite hangs the gouda Jenkins build slave

  - Couldn't install application: error seen when trying to install to
    target using ade

  - Ribchester mount unit depends on Btrfs

  - Access denied message seen when we shutdown an i.MX6 or Minnowboard
    max target

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on
    \`mx6qsabrelite\` board

  - Seek/Pause option does not work correctly on YouTube

  - Failed unmounting /var bind mount error seen on shutting down an
    i.MX6/Minnowboard with Minimal ARM-32/AMD64 image

  - sqlite-veryquick: analyzer1 cannot run because ./sqlite3_analyzer
    is not available

  - Apps like songs, artists etc are not offser correctly by the
    compositor window

  - Double tapping on any empty space in the songs app's screen results
    in the compositor window hinding the left ribbon

  - Back button is hidden by the compositor window in apps like songs,
    artists etc

  - Ensure that the uploaded linux package version uniquely identifies a
    single commit

  - Cantebury test case is missing

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - Mildenhall compositor gets offset when we open an app like songs

  - Connman fails to connect when hotplugging Wifi dongle

  - Wifi needs disabling and then enabling after boot to get it working

### Low (115)

  - Remove unnecessary folks package dependencies for automated tests

  - Not able to load heavy sites on GtkClutterLauncher

  - No connectivity Popup is not seen when the internet is disconnected.

  - Upstream: linux-tools-generic should depend on lsb-release

  - remove INSTALL, aclocal.m4 files from langtoft

  - Mildenhall compositor crops windows

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Network search pop-up isn't coming up in wi-fi settings

  - mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - Back option is missing as part of the tool tip

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - The video player window is split into 2 frames in default view

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - Unusable header in Traprain section in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - gstreamer1-0-decode: Failed to load plugin warning

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - Album art is missing in one of the rows of the songs application

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Status bar is not getting updated with the current song/video being
    played

  - Compositor hides the other screens

  - playback system clutter-gst-playback init() system is crashing due
    to which songs app is not launching again

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - "connman: patch ""Use ProtectSystem=true"" rejected upstream"

  - "connman: patch ""device: Don't report EALREADY"" not accepted
    upstream"

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - zoom feature is not working as expected

  - Segmentation fault is observed on closing the mildenhall-compositor

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - Blank screen seen on executing last-resort-message testcase

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - folks-telepathy: folks-retrieve-contacts-telepathy_sh.service
    failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-folks: Several tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - didcot: test_open_uri: assertion failed

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - frome: \`/tmp/frome-\*': No such file or directory

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - webkit2gtk-drag-and-drop doesn't work with touch

  - Icons for Apps like Music Settings are missing on the OSTree based
    images
