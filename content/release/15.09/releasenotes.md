+++
date = "2015-12-02"
weight = 100

title = "15.09 ReleaseNotes"

aliases = [
    "/old-wiki/15.09/ReleaseNotes"
]
+++

# Apertis 15.09 Release

**15.09** is the current development distribution of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat API) and
Intel x86 (64/32-bit Intel) systems. Features which are planned for
Apertis can be found on the [Roadmap]( {{< ref "/roadmap.md" >}} ) page.

### What's available in the distribution?

The software stack in **15.09** is comprised of the following
technologies:

  - Linux 3.19
  - Graphical subsystem based on X.org X server 1.17.1 and Clutter
    1.22.2 with full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.27, BlueZ 4.101
    and Ofono 1.14
  - Multimedia support provided by GStreamer 1.0
  - The Telepathy framework, with XMPP and SIP support
  - The Folks contact management framework
  - The Clutter port of the WebKit browser engine with support for WebGL

### What's new in the distribution?

  - Scaled down Apertis Images
  - Migration to WebKit2GTK+
  - AppArmor profiles tightened up
  - System services in their own cgroup
  - Newport (download manager) design review
  - Media and indexing updates
  - Automated test updates
  - Sensors and Actuators design
  - JSON testing with Walbottle
  - UI customisation design
  - Barkway (notifications service) design review
  - Documentation infrastructure improvements
  - Security design
  - Application-bundle design
  - Application communication designs

### Release downloads

| [Apertis 15.09 images](https://images.apertis.org/release/15.09/) |
| ----------------------------------------------------------------- |
| Intel 32-bit                                                      |
| Intel 64-bit / Minnowboard MAX                                    |

  - Apertis 15.09 repositories:

` deb `<https://repositories.apertis.org/apertis/>` 15.09 target development sdk hmi`

  - Apertis 15.09 infrastructure tools:

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

# Apertis 15.09 Release

This release builds on the Ubuntu 15.04 (Vivid) as base distribution.
The 15.09 release has been verified in the [15.09 test
report](). A later [bug fix
(\#429)]() for webkit-clutter has been included and
tested separately after test run, therefore released images have
different versioning than the ones from test report.

## Scaled down Apertis Images

During the 15.09 release a scaled down version of Apertis was created.
The goal of this version is to show Apertis can be used in situations
where no HMI is required and Apartis would run with a very low amount of
resources to e.g. serve as a bridge between the automotive world and the
wider internet.

A description explaining how this image was put together can be found on
the [ScaledDownApertisImage]( {{< ref "/scaleddownapertisimage.md" >}} ) page.

## Migration to WebKit2GTK+

The WebKit2GTK+ engine is feature complete and the package has been made
available in the target section of Apertis. To try it out, install the
*webkit2gtk-testing* package and run the GtkClutterLauncher test browser
that ships with it. The API can be used by installing the
*libwebkit2gtk-4.0-dev* package. Details can be found on the
[WebKit2]( {{< ref "/webkit2.md" >}} ) page, the code can be found in the git
repository:

<https://git.collabora.com/cgit/webkit-clutter.git/tree/?h=gtk-clutter>

It is critical at this point that all developers and components using
the WebKit Clutter engine try out the new packages to report issues and
start planning their migration to the new engine.

## AppArmor profiles tightened up

An effort has been started to audit and improve the
[AppArmor]() profiles for the various
Apertis application bundles currently on
[git.apertis.org](https://git.apertis.org). For the 15.09 release, this
involves ensuring that for every program started during boot has an
AppArmor profile, the profile correctly allows the program's normal
activity and does not log "complaints" about actions which are not
allowed by the profile. This is a prerequisite for having the program
continue to work correctly when its profile is switched to enforcing
mode.

After the 15.09 release, the plan is to continue with switching profiles
to enforcing mode, so that they provide security for these programs. The
application manager Canterbury will be the first to become enforcing,
since this affects its ability to start application bundles correctly.

This partially addresses bugs , , , , , , , ,  and .

## System services in their own cgroup

All activatable D-Bus system services in the 15.09 release are managed
by 
[systemd]( {{< ref "/architecture/boot_process.md" >}} ).
Previously, the system dbus-daemon would have started those services
directly, which meant that platform components such as systemd viewed
them as logically part of dbus-daemon.

Services that were affected by this improvement include the `pacrunner`
proxy auto-configuration service, the `tcmmd` traffic-shaping service,
the `chaiwala-btrfs-updater` platform upgrade service, the
`wpa_supplicant` wireless LAN service, the `blueman` Bluetooth UI used
in SDK images, and the system download manager used by platform
upgrades.

This means that each of the affected services runs in a cgroup (resource
control group), which affects resource allocation: resources such as CPU
time can be shared evenly between cgroups. Previously, the system
dbus-daemon and all of the affected services were scheduled together, so
they could potentially "starve" each other. In future, each of the
affected services can also have its own resource consumption limits and
other sandboxing measures set by systemd configuration, if required.

After the 15.09 release, this will be extended to cover per-user
services as well as system services, and dbus-daemon will be
reconfigured so that D-Bus services that are not properly managed by
systemd will not be supported. This change will also allow the removal
of a setuid executable, reducing the risk of security vulnerabilities.

## Newport (download manager) design review

There has been an extended period of API and security review on the
design of the [Newport download manager](),
including a series of patches which rearchitect its D-Bus API.

Discussion about the Newport API and security review can be found on
bugs  and . The corresponding code changes have been reviewed, but could
not be merged in time for the 15.09 freeze.

## Media and indexing updates

Focus has been placed on updating, testing and fixing various bugs in
the media and indexing software: libmediaart (2.0 transition), Tumbler,
grilo, grilo-plugins, Tracker, and components of GLib. This has resulted
in improved test cases and better alignment with the thumbnailing
standards. The Apertis Media and Indexing design has been updated
accordingly, [to version
0.5.3](/images/apertis-media-management-design-0.5.3.pdf).

## Automated test updates

Many automated test-cases have been improved to be more reliable, and
provide better diagnostics on failure.

The automated test-cases are now run in an environment that more closely
reflects how programs are run on real devices, avoiding some situations
where test results might not match whether a particular feature works in
production.

Timeout behaviour has been improved: if a test does not finish within
the time allowed, the automated test framework will terminate it, record
it as a failure and move on to the next test. This avoids some error
situations where a test that timed out would result in subsequent tests
not being attempted.

The Tracker test cases have been ported from Bash to Python, which has
improved reliability and test execution time.

Some additional test-cases have been added, including simple, fast
checks for basic platform functionality.

## Sensors and Actuators design

A new Sensors and Actuators design document has been produced,
[currently at version
0.2.2](/images/apertis-sensors-actuators-design-0.2.2.pdf).
This provides the basis for a full sensors and actuators API proposal in
the next release — currently the overall use cases, design requirements,
architecture and roadmap have been planned and reviewed. The API will
allow application bundles to access sensor devices in the vehicle, to
give them some input from their environment; and to have limited control
of actuators (for example, seat adjustment) in a safe manner.

## JSON testing with Walbottle

Improvements have been made to the core of
[Walbottle](https://github.com/pwithnall/walbottle), a unit test
generator for [JSON data formats]( {{< ref "json_parsing.md" >}} ),
to allow it to be used more for testing Apertis SDK libraries. Walbottle
has been integrated with libthornbury, a UI utility library. Further
improvements will be made to it after the 15.09 release, and it will be
integrated with more libraries to improve test coverage further.

## UI customisation design

The UI customisation design has been discussed and reviewed, and a plan
for using CSS to customise the appearance of application UIs has been
agreed. This involves using the mature CSS functionality from GTK+ to
style Clutter widgets. Appropriate integration code has been written to
allow this, including a demonstration application which shows how to use
GTK+ CSS APIs with Clutter.

Discussion happened in [bug
\#364](https://bugs.apertis.org/show_bug.cgi?id=364).

## Barkway (notifications service) design review

There has been ongoing review of the Barkway notifications service
design, trying to determine the best approach for displaying
notifications and dialogues from multiple applications and background
services, and how best to integrate this with the planned Wayland
compositor. Planning of this functionality will continue after the 15.09
release.

## Documentation infrastructure improvements

Work is ongoing on
[hotdoc](https://github.com/MathieuDuponchelle/hotdoc) to make it fit
the requirements for the Apertis documentation system, replacing
gtk-doc, which does not support documenting JavaScript or Python APIs.

Multi-language output has been implemented: the tool now generates
documentation for the original C API, plus JavaScript and Python
bindings. Various libraries are being ported, with libclapton being used
as a testbed, and changes to it due to land after the 15.09 release.
Work has started on allowing hotdoc to document D-Bus interfaces using
[dbus-deviation](https://people.collabora.com/~pwith/dbus-deviation/).

The plan for 15.12 is to port all other Apertis modules to hotdoc.

## Security design

The [Security design document]( {{< ref "security.md" >}} ) has been
updated to define the relevant terms, document several aspects of the
threat model and the security policies that result from it, update
external references, and describe how polkit (formerly PolicyKit) is
used in the Apertis system.

## Application-bundle design

Work has continued on defining the structure of application bundles,
application entry points and agents. A need for a multi-user-compatible
structure for the on-disk layout of applications has been identified; a
proposal for this is being written and will be published after the 15.09
release.

## Application communication designs

Draft designs for [interface discovery]( {{< ref "/interface_discovery.md" >}} )
and [data sharing]( {{< ref "/data_sharing.md" >}} ) have been added to the
Apertis wiki. These provide generic "building blocks" for app-to-app
communication. A draft design for handling [points of
interest]( {{< ref "/points_of_interest.md" >}} ) is a new feature in its own
right, and also acts as a case-study for a specific set of use-cases for
app-to-app communication.

Concrete use-cases for [content hand-over]( {{< ref "/concepts/content_hand-over.md" >}} ) and
[sharing]( {{< ref "/concepts/sharing.md" >}} ) have also been collected for use in
future designs.

## Infrastructure

### Updated packages

During Q3 cycle, several activities have been carried out to be able to
rebase Apertis against new upstream base (Ubuntu Vivid Vervet), updating
the platform with new features and updated for bug and security fixes. A
total of 195 packages have been modified out of 507 in target component;
143 packages out of 1106 in development component; and 70 out of 452 in
SDK. In addition to base distribution, HMI packages have also been
updated to their latest stable version.

### OBS Build Projects

  - [OBS Apertis 15.09
    Target](https://build.collabora.co.uk/project/show?project=apertis%3A15.09%3Atarget)
  - [OBS Apertis 15.09
    Development](https://build.collabora.co.uk/project/show?project=apertis%3A15.09%3Adevelopment)
  - [OBS Apertis 15.09
    SDK](https://build.collabora.co.uk/project/show?project=apertis%3A15.09%3Asdk)
  - [OBS Apertis 15.09
    HMI](https://build.collabora.co.uk/project/show?project=apertis%3A15.09%3Ahmi)
  - [OBS Apertis Infrastructure
    Tools](https://build.collabora.co.uk/project/show?project=apertis%3Ainfrastructure)

### Shared Repositories

Repositories are found at:

` deb `<https://repositories.apertis.org/apertis/>` 15.09 target development sdk hmi`

To be able to access those, a temporary user/passwd has been created
until properly publicly published: Username: **apertis-dev** Password:
**Apa8Uo1mIeNgie6u**

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

*Use your Collabora credentials to access those until those get publicly
published.*

Image build tools can be found in the Apertis tools repositories. Note
that a string is added to package version depending on the distribution
suite based on. For example, if trusty system is to be used expect to
install image-builder_7trusty1

|                                                                                                                           |                  |
| ------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| **Package**                                                                                                               | **Version**      |
| [image-builder, image-tools](https://git.apertis.org/cgit/image-utils.git/)                                               | 7                |
| [apertis-image-configs, apertis-image-scripts (\*)](https://git.apertis.org/cgit/apertis-image-customization.git/)        | 14               |
| [linaro-image-tools, python-linaro-image-tools](https://git.collabora.com/cgit/singularity/tools/linaro-image-tools.git/) | 2012.09.1-1co38  |
| parted, libparted0debian1                                                                                                 | 2.3-11ubuntu1co3 |
| python-debian                                                                                                             | \>=0.1.25        |

### Test Framework

LAVA service at Collabora triggers test cases upon image builds, service
is found at:

` `<https://lava.collabora.co.uk/>

The list of available test cases, including those can be found
[here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
be able to install it, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation)

## Known issues

  - Failing test:
    [traffic-control-tcmmd]( {{< ref "/qa/test_cases/traffic-control-tcmmd.md" >}} )
    ([bug \#327]())
  - Failing test:
    [apparmor-libreoffice]( {{< ref "/qa/test_cases/apparmor-libreoffice.md" >}} )
    ([bug \#331]())
  - factory-reset-tool TC: flagcheck messages are hidden by Plymouth
    ([bug \#386]())
  - Roller performance issues: Kinetic scrolling with
    MxKineticScrollView (and any roller widget, which uses it
    internally) may fail if the scroll view contains many child widgets
    (for example, over 100). A workaround is to reduce the number of
    child widgets; a more permanent fix is being worked on. ([bug
    \#401]())
  - Boot chart failed to generate ([bug \#467]())
  - Gstreamer-buffering: Videoplayer screen is transparent partially
    ([bug \#498]())
  - tracker-indexing-mass-storage:Test fails ([bug
    \#500]())
  - librest-unit: lastfm test failed in all platforms ([bug
    \#509]())
  - apparmor-bluez-setup: Apparmor complaints found ([bug
    \#511]())
  - apparmor-bluez-avrcp-volume: Apparmor complaints ([bug
    \#512]())
  - apparmor-webkit-clutter: normal/malicious expected_underlying tests
    fail ([bug \#514]())
  - apparmor-pulseaudio: several tests failed for ARM image only ([bug
    \#515]())
