+++
date = "2020-12-03"
weight = 100

title = "v2020.4 Release schedule"
+++

The v2020.4 release cycle started in December 2020.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2020-12-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2021-03-05        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2021-02-24        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2021-03-03        |
| RC testing                                                                                               | 2021-03-04..03-10 |
| v2020.4 release                                                                                          | 2021-03-11        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
