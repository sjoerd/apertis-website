+++
date = "2019-12-12"
weight = 100

title = "V2020pre ReleaseNotes"

aliases = [
    "/old-wiki/V2020pre/ReleaseNotes"
]
+++

# Apertis v2020pre Release

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2020pre** is the **preview** release of the Apertis v2020
stable release flow that will lead to the LTS **Apertis v2020.0**
release in March 2020.

This Apertis release is built on top of Debian Buster with several
customisations.

Test results for the v2020pre release are available in the following
test reports:

  - [APT
    images](https://lavaphabbridge.apertis.org/report/v2020pre/20191205.0)
  - [OSTree
    images](https://lavaphabbridge.apertis.org/report/v2020pre/20191205.0/ostree)
  - [NFS
    artifacts](https://lavaphabbridge.apertis.org/report/v2020pre/20191205.0/nfs)
  - [LXC
    containers](https://lavaphabbridge.apertis.org/report/v2020pre/20191205.0/lxc)

## Release flow

  - 2019 Q3: v2020dev0
  - 2019 Q4: **v2020pre**
  - 2020 Q1: v2020.0
  - 2020 Q2: v2020.1
  - 2020 Q3: v2020.2
  - 2020 Q4: v2020.3
  - 2021 Q1: v2020.4
  - 2021 Q2: v2020.5
  - 2021 Q3: v2020.6
  - 2021 Q4: v2020.7

### Release downloads

| [Apertis v2020pre images](https://images.apertis.org/release/v2020pre/) |
| ----------------------------------------------------------------------- |
| Intel 64-bit                                                            |
| ARM 32-bit (U-Boot)                                                     |
| ARM 64-bit (U-Boot)                                                     |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2020pre repositories

` $ deb `<https://repositories.apertis.org/apertis/>` v2020pre target development sdk hmi`

## New features

### Linux kernel 5.4

Released upstream on the 2019-11-24, a bit more than two weeks ago, the
latest LTS version of the Linux kernel is now included in this release.
With a minimum projected end-of-life in December 2021, the 5.4 series
better suits the v2020 release cycle, which would reach its own
end-of-life at roughly the same time (2021 Q3).

### Persistent kernel panic storage

This release introduces initial support for
[pstore](https://www.kernel.org/doc/Documentation/ABI/testing/pstore) in
the U-Boot bootloader and the Linux kernel to debug hard crashes by
providing a way to store logs for postmortem analysis (specifically next
boot) even if the crash makes the main persistent storage no longer
available or unsafe to access.

A new document [guides developers through the steps needed to use pstore
on the
i.MX6]( {{< ref "pstore.md" >}} )
Sabrelite boards.

### hawkBit integration proof-of-concept

[Eclipse hawkBit™](https://www.eclipse.org/hawkbit/) is a back-end
framework for deploying software updates over the Internet, providing
advanced phased rollout management.

A proof-of-concept hawkBit server instance has been deployed for Apertis
and the image building pipelines now [automatically push updates to
it](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/merge_requests/187)
as OSTree static bundles.

The Apertis v2020pre OSTree images now ship the [Apertis hawkBit
agent](https://gitlab.apertis.org/appfw/apertis-hawkbit-agent/)
component by default, which can be enabled to let developers control
updates using the hawkBit server instance.

The [Deployment management
document]( {{< ref "deployment-management.md" >}} )
describes how the updates can be modeled on the hawkBit server and how
to set up agents to connect to it and deploy the updates on the device.

## Build and integration

### Early license scans with GitLab CI

The GitLab CI pipeline used to build source packages and upload them to
OBS now also [checks the sources for
licenses](https://gitlab.apertis.org/infrastructure/ci-package-builder/merge_requests/27)
that are [not suitable for
Apertis]( {{< ref "license-expectations.md" >}} )
to let developers catch issues as early as possible.

## Quality Assurance

### Whole-archive package installability checks

A new set of health checks has been deployed for this release: the
[installability
checks](https://gitlab.apertis.org/infrastructure/health-checks/merge_requests/4)
now tests that all the relevant packages dependencies can be satisfied
in the Apertis archive.

This is important because Apertis only ships a subset of the upstream
Debian archive and on top of that some repositories have further
restrictions that purposely make some of the binaries they ship not
installable unless other repositories are also available: for instance
`gcc` is part of the `target` repository but only libraries like
`libgcc` have to be installable on their own, tools like the compiler
itself may depend on other packages that are part of the `development`
repository.

## Design and documentation

### Tracking DUT-local resources consumption during tests on LAVA

A new repository contains documentation and examples on how to write
tests that can be used to [track and report system resources during
automated tests
execution](https://gitlab.apertis.org/docs/lava-tests-monitoring/) in
LAVA.

Multiple metrics can be captured for a single test run, each composed of
a single value, for instance:

  - total execution time
  - total CPU consumption
  - maximum RAM resident-set size
  - bandwidth consumption
  - number of context switches

### Using LAVA parallel pipelines to interact with resources that are external to the DUT

The [LAVA External Device
Monitoring]( {{< ref "lava-external-devices.md" >}} )
document describes how to execute automated LAVA tests controlling
resources external to the DUT across a network implementing a LAVA
parallel pipeline job.

The approach proposed helps to address test cases like:

  - Executing a test in the DUT where certain power states are simulated
    (for example a power loss) during specific test actions using a
    programmable PSU external to the DUT.
  - Executing a test in the DUT simulating SD card insertion and removal
    using an external device.

The only assumption, in both scenario, is that the external device
(either a programmable PSU or SD-card simulator) can be accessed through
the network using SSH.

### Hosting the whole build infrastructure on Intel x86-64-only providers

The Apertis OBS instance relies on native ARM 32 and 64 bit machines as
workers to build packages. This ensures the best performance but can be
problematic for downstream that want to host their infrastructure in the
cloud, as not all cloud providers offer ARM servers.

The [Build infrastructure on Intel
x86-64]( {{< ref "x86-build-infrastructure.md" >}} )
document explores the alternatives to native workers and the challenges
involved.

### Secure boot

For both privacy and security reasons it is important for modern devices
to ensure that the software running on the device hasn't been tampered
with. In particular any tampering with software early in the boot
sequence will be hard to detect later while having a big amount of
control over the system. To solve this issues various vendors and
consurtiums have created technologies to combat this, known under names
as "secure boot", "highly assured boot" (NXP), "verified boot" (Google
Android/ChromeOS).

While the scope and implementation details of these technologies differs
the approach to provide a trusted boot chain tends to be similar between
all of them. The [Apertis secure
boot]( {{< ref "secure-boot.md" >}} ) document
discusses how that aspect of the various technologies works on a
high-level and how this can be introduced into Apertis.

### Developing on top of Apertis kernel

A new developers handbook helps people interested in [building, patching
and maintaining versions of the Linux
kernel]( {{< ref "buildingpatchingandmaintainingtheapertiskernel.md" >}} )
based on the Apertis package.

The guide suggests workflows both utilising the Debian infrastructure on
which Apertis is built and a workflow that eschews the Debian
infrastructure and thus is usable by third party integrators who may
need access to a patched kernel tree for integration, but who's workflow
would make using the Apertis infrastructure difficult.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Breaks

No incompatibility compared to the previous release is known.

## Infrastructure

### Apertis Docker registry

The Apertis Docker registry stores Docker images in order to provide a
unified and easily reproducible build environment for developers and
services.

As of today, this includes the `apertis-image-builder`,
`apertis-package-builder`, `apertis-package-source-builder`,
`apertis-testcases-builder` and `apertis-documentation-builder` Docker
images.

### Apertis infrastructure tools

The [Apertis v2020 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2020)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

` deb `<https://repositories.apertis.org/infrastructure-v2020/>` buster infrastructure`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (7)

  - RBEI FTBFS: apitrace

  - USB adapter start to reconnect breaking the configuration

  - Touch functionality does not work on the arm32/armhf public image

  - Songs/Videos don't play on i.MX6 with Frampton on the arm32/armhf
    public image

  - VirtualBox guest additions do not (yet) work with linux 5.4

  - ARM32 Public OSTree based Image does not boot up when connected with
    the LCD Display

  - Cross-toolchain tarball pipeline is failing

### Normal (137)

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Crash when initialising egl on ARM target

  - Develop test case for out of screen events in Wayland images

  - Test apps are failing in Liblightwood with the use of GTest

  - Fix Tracker testcase to not download media files from random HTTP
    user folders

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - libgles2-vivante-dev is not installable

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Ensure that the arch:all packages in the archive match the
    arch-specific versions

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - gupnp-services: browsing and introspection tests fail

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - SDK hangs when trying to execute bluez-hfp testcase

  - Ribchester mount unit depends on Btrfs

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on the internal
    mx6qsabrelite images with proprietary kernel

  - Seek/Pause option does not work correctly on YouTube

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - cgroups-resource-control: cpu-shares test failed

  - tracker-indexing-local-storage: run-test test failed

  - apparmor-tracker: tracker.normal.expected test failed

  - gupnp-services: test_service_introspection test failed

  - gupnp-services: test_service_browsing test failed

  - build failure in liblightwood-2 on 18.06 image

  - "Setup Install to Target" option is not showing on eclipse

  - rhosydd: integration test failed

  - build-snapshot: allow to build packages without \`autogen.sh\`
    script

  - Wi-Fi search button is missing in wifi application

  - traprain: gnome-desktop-testing test failed

  - bluez-hfp testcase fails

  - \`kbd\` package has broken/problematic dependencies

  - Debos crash when a recipes embbed recipes with ostree-commit and
    ostree-deploy

  - Both sysroot and devroot folder is exist in /opt path

  - The /boot mountpoint is not empty

  - System users are shipped in /usr/etc/passwd instead of /lib/passwd

  - apparmor-gstreamer1-0: run-test-sh test failed

  - Fix the RFS tiny images filesystem structure

  - apparmor-gstreamer1-0: 6_apparmor-gstreamer1-0 test failed

  - cgroups-resource-control: 8_cgroups-resource-control test failed

  - folks-alias-persistence: 4_folks-alias-persistence test failed

  - No test-text is displayed inside the Xephyr window.

  - aa_get_complaints.sh script needs to be run with sudo permission

  - apparmor-tumbler: 9_apparmor-tumbler test failed

  - cgroups-resource-control: 6_cgroups-resource-control test failed

  - gettext-i18n: gettext-i18n test failed

  - apparmor-tumbler: 8_apparmor-tumbler test failed

  - apparmor-utils: apparmor-utils test failed

  - gettext-i18n: 11_gettext-i18n test failed

  - tracker-indexing-local-storage: 14_tracker-indexing-local-storage
    test failed

  - canterbury: 3_canterbury test failed

  - Terminal comes up inside the Launcher

  - gettext-i18n: 10_gettext-i18n test failed

  - tracker-indexing-local-storage: 13_tracker-indexing-local-storage
    test failed

  - tracker-indexing-local-storage: 17_tracker-indexing-local-storage
    test failed

  - dbus-installed-tests: trying to overwrite mktemp.1.gz

  - Songs/Videos don't play on i.MX6 with Frampton on internal images

  - sdk-dbus-tools-bustle testcase is failing

  - apparmor-ofono test fails

  - apparmor-bluez-avrcp-volume test fails

  - evolution-sync-bluetooth test fails

  - eclipse-plugins-apertis-management package is missing

  - A2DP test is failing as part of the bluez-phone test

  - No audio/sound is heard while making a call in the bluez-hfp
    testcase

  - Video does not stream in WebKit on the i.MX6 internal images

  - ofono-tests package is missing

  - connman-pan-tethering test fail

  - connman-pan-network-access test fails

  - connman-usb-tethering test fails

  - ifconfig command need to be run with sudo permission

  - libfolks-ofono25 package not found

  - canterbury: 2_canterbury test failed

  - newport: 7_newport test failed

  - newport: 6_newport test failed

  - rhosydd: 8_rhosydd test failed

  - rhosydd: 7_rhosydd test failed

  - traprain: 10_traprain test failed

  - traprain: 9_traprain test failed

  - eclipse-plugins-remote-debugging test fails

  - Thumb nails of songs are not seen on the SDK

  - The pacrunner package used for proxy autoconfiguration is not
    available

  - Auto-resizing of the SDK display in VirtualBox is known to be broken

  - Booting the SDK in VirtualBox appears to be broken with the 6.0.4
    VirtualBox Guest Extensions

  - webkit2gtk-event-handling-redesign test fails on the amd64 ostree
    images

  - simple-agent not found

  - bluez-avrcp-volume test fails on the sdk and base sdk

  - bluez-hfp testcase fails on sdk

  - folks-inspect: command not found

  - Multimedia playback is broken on the internal i.MX6 images (internal
    3.14 ADIT kernel issue)

  - gupnp-services: 13_gupnp-services test failed

  - youtube Videos are not playing on upstream webkit2GTK

  - Page scroll is lagging in Minibrowser on upstream webkit2GTK

  - Disable timesyncd on images and set fallback time servers for
    ConnMan

  - traprain: 7_traprain test failed

  - frome: 5_frome test failed

  - frome: 6_frome test failed

  - qa-report-app: Tests results are not available for the modules job

  - gitlab-to-obs: Handle packages changing component across releases

  - apparmor-session-lockdown-no-deny test fails on all platforms

  - AppArmor ubercache support is no longer enabled after 18.12

  - ldconfig: Warning comes up when we do an apt-get upgrade on the
    i.MX6

  - Generated lavaphabbridge error report email provides wrong link for
    full report link

  - gettext-i18n: 9_gettext-i18n test failed

  - tracker-indexing-local-storage: 16_tracker-indexing-local-storage
    test failed

  - gupnp-services: 11_gupnp-services test failed

  - apparmor-pulseaudio: 14_apparmor-pulseaudio test failed

  - apparmor-tracker: 21_apparmor-tracker test failed

  - tracker-indexing-local-storage: 20_tracker-indexing-local-storage
    test failed

  - apparmor-tracker: 20_apparmor-tracker test failed

  - tracker-indexing-local-storage: 19_tracker-indexing-local-storage
    test failed

  - sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint
    test failed

  - sdk-debug-tools-gdb: 4_sdk-debug-tools-gdb test failed

  - sdk-debug-tools-strace: 5_sdk-debug-tools-strace test failed

  - sdk-debug-tools-valgrind: 6_sdk-debug-tools-valgrind test failed

  - sdk-cross-compilation: 10_sdk-cross-compilation test failed

  - apparmor-gstreamer1-0: 12_apparmor-gstreamer1-0 test failed

  - aum-update-rollback-tests/arm64,amd64: Automatic power cut test
    should be reworked to reduce the speed of delta read

  - aum-update-rollback-tests/amd64: DNS not available in LAVA tests
    after reboot

  - Thumbnails for songs/videos are not shown when opening the app too
    soon

  - assertion 'error == NULL || \*error == NULL' failed message seen on
    executing apertis-update-manager-diskfull testcase

  - Ostree upgrade failed message seen when executing
    apertis-update-manager-diskfull test

  - aum-update-rollback-tests/armhf: Rollback situation is not
    reproduced on public armhf target and internal images

  - Repeatedly plugging and unplugging a USB flash drive on i.MX6
    (Sabrelite) results in USB failure

  - apparmor-geoclue: 8_apparmor-geoclue test failed

  - apparmor-geoclue: 7_apparmor-geoclue test failed

  - Remove Packages files for ARM architectures

  - bluez-phone test fails

  - A2DP test is failing as part of the bluez-phone test

### Low (19)

  - Upstream: linux-tools-generic should depend on lsb-release

  - telepathy-ring: Review and fix SMS test

  - Mildenhall compositor crops windows

  - Power button appers to be disabled on target

  - Network search pop-up isn't coming up in wi-fi settings

  - Videos are hidden when Eye is launched

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - The video player window is split into 2 frames in default view

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - Status bar is not getting updated with the current song/video being
    played

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault is observed on closing the mildenhall-compositor

  - webkit2gtk-drag-and-drop doesn't work with touch

  - traffic-control-basic test fails

  - apt-get dist-upgrade fails on SDK

### Lowest (82)

  - Remove unnecessary folks package dependencies for automated tests

  - No connectivity Popup is not seen when the internet is disconnected.

  - remove INSTALL, aclocal.m4 files from langtoft

  - Documentation is not available from the main folder

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - Unusable header in Traprain section in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Compositor hides the other screens

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - "connman: patch ""Use ProtectSystem=true"" rejected upstream"

  - "connman: patch ""device: Don't report EALREADY"" not accepted
    upstream"

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - zoom feature is not working as expected

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - libsoup: /usr/lib/libsoup2.4/installed-tests/libsoup/ssl-test test
    failed

  - cgroups-resource-control: 3_cgroups-resource-control test failed

  - tracker-indexing-local-storage: 18_tracker-indexing-local-storage
    test failed

  - frome: 6_frome test failed

  - frome: 5_frome test failed
