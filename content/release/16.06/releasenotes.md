+++
date = "2016-09-15"
weight = 100

title = "16.06 ReleaseNotes"

aliases = [
    "/old-wiki/16.06/ReleaseNotes"
]
+++

# Apertis 16.06 Release

**16.06** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat ABI) and
Intel x86 (64/32-bit Intel) systems. Features which are planned for
Apertis can be found on the [Roadmap]( {{< ref "/roadmap.md" >}} ) page.

### What's available in the distribution?

The software stack in **16.06** is comprised of the following
technologies:

  - Linux 4.4
  - Graphical subsystem based on Wayland 1.10.0 and Clutter 1.24.2 with
    full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.32, BlueZ 5.37 and
    Ofono 1.17
  - Multimedia support provided by GStreamer
  - The Clutter port of the WebKit browser engine with support for WebGL
  - The GTK+ port of WebKit with enhancements from WebKit Clutter ported
    to it and a ClutterActor wrapper

### What's new in the distribution?

  - Application management improvements
  - Security design assessment
  - Implementation of Sensors and Actuators (Rhosydd)
  - Switch from X to Wayland by default on all images except SDK
  - Rebase of Apertis on the latest Ubuntu upstream release 16.04
    (Xenial Xerus)
  - Webkit2GTK+ performance optimizations for Wayland
  - Webkit2GTK+ performance optimizations for WebGL
  - Updates to Apertis sites look & feel
  - [Documentation portal](https://docs.apertis.org/design/index.html)
  - [Application developer portal](https://appdev.apertis.org)

### Release downloads

| [Apertis 16.06 images](https://images.apertis.org/release/16.06/) |
| ----------------------------------------------------------------- |
| Intel 32-bit                                                      |
| Intel 64-bit / Minnowboard MAX                                    |

#### Apertis 16.06 repositories

` deb `<https://repositories.apertis.org/apertis/>` 16.06 target helper-libs development sdk hmi`

#### Apertis 16.06 infrastructure tools

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

# Apertis 16.06 Release

This Apertis release is based on top of Ubuntu 16.04 (Xenial) release as
its base distribution. Test results of the 16.06 release are available
in the [16.06 test report]().

## Application management

### [Application Layout]( {{< ref "application-layout.md" >}} )

Following on from the the [Application
Layout]( {{< ref "application-layout.md" >}} ) design work done in 16.03, the
Ribchester subvolume manager now separates the read-only part of
application bundles (`/Applications`, `/usr/Applications`) from the
variable part (`/var/Applications`). As part of this layout change,
bundle manipulation is done using atomic filesystem operations, so that
power loss during app management does not result in corruption.

A new tool `ribchester-bundle` can be used to manipulate store
application bundles during development and rapid prototyping.
`ribchester-bundle` is not intended to be used in production: the same
operations should be carried out directly from C code, rather than using
this tool.

Work has begun on defining per-application-bundle metadata; this will
continue in the 16.09 cycle.

### Multi-user support

The Ribchester subvolume manager no longer creates directories owned or
writeable by the `user` user ID representing the user, except for those
directories solely intended for use by that specific user. If a
different user ID runs an application bundle, Ribchester will correctly
create a separate data directory for each user ID.

### Automated tests

The Ribchester subvolume manager now has test coverage for common use
cases when dealing with store application bundles: install, upgrade,
remove, roll back and so on.

## Security design

Several potential improvements to the [Security concept
design](https://docs.apertis.org/design/security.html) have been
collected from other automotive projects' security white papers, and
will be incorporated into the concept designs in future releases if time
permits. These include:

  - Document threat models with/without physical access

  - Revisit app-bundle packaging, signing, manifest and authentication
    plans

  - Describe security implications of dependency handling

  - Document why we don't necessarily need to check app integrity on
    launch

  - Document why we need to verify files on removable storage while
    streaming

  - Mention privacy protection (resistance to spying) in Security
    concept design

  - Concept design for detecting and reporting potential attacks

  - Concept design: security vs. debugging

  - Concept design for content control

  - Mention CSP and other web security technologies in Security concept
    design

  - Concept design: security implications of expanding system storage
    onto SD card

  - Document security implications of crafted removable media (e.g. USB)

  - Specifically say that playlists, media etc. imported from removable
    devices must be distrusted

  - Discuss options for booting from USB

  - Security concept design: include qualitative references to desirable
    privacy goals

  - Concept designs: write about the need for security with
    very-long-term support

  - Security concept design: state general principles

  - Security concept design: describe the "rental car" use case

  - Document why we prefer to use D-Bus

  - Compare models for signing archive/container files

In addition, based on the same sources, some potential hardening work
has been planned for future releases:

  - Document recommended compiler hardening options for TCB projects

  - Disable ptrace for users

  - Document the debug features that should (not) be turned off for
    production

  - Hard-code dbus-daemon to not offer TCP or ANONYMOUS auth

and some potential improvements to automated testing have been planned
for future releases:

  - Add test to ensure /usr is read-only

  - Check and test kernel hardening options

  - New test: try to connect to session/system D-Bus without correct
    EXTERNAL auth, assert that we fail

  - New test: port-scan a target/development image, assert that no
    unexpected ports are open

## Sensors and actuators

Rhosydd, an implementation of the non-vendor-specific and
non-product-specific parts of the [Sensors and Actuators concept
design](https://docs.apertis.org/design/sensors-and-actuators.html), is
available in Apertis 16.06.

## WebKit2GTK+

The 16.06 release includes Wayland support along with the
WebKitWebClutterView actor for integration in Apertis applications. The
dependency on the clutter-gtk library was removed by streamlining
rendering and event handling. Several of the Apertis-specific APIs are
also available, but this structural work caused some regressions. See
the [known issues]( {{< ref "/release/16.06/releasenotes.md#known-issues" >}} ) section
below for a list of regressions.

## Wayland by default

Wayland is now the default graphic server for target and development
images. Initially supported since late last year, Wayland is now the
preferred option on all images, with the exception of the SDK.

## [Documentation portal](https://docs.apertis.org/design/index.html)

The design documentation has been moved out of the wiki into its own
portal. Also, the files were converted to markdown format and moved to a
[git repository](https://git.apertis.org/cgit/apertis-designs.git/),
explicitely setup for this purpose. This way, documentation updates and
review are now handled using Differential in Phabricator, in the same
way as source code. This has standardised and improved the review
process and the traceability of changes.

## [Application developer portal](https://appdev.apertis.org)

This is a portal focused on application developers for Apertis. Among
other features, it contains guides and tutorials, which have been
converted to markdown format and moved to
[git](https://git.apertis.org/cgit/apertis-docs.git/). The update and
review process has been integrated into Phabricator, like the design
documentation. It is an initial release of the application developer
portal. The work continues to add more features and content. The source
code is available [here](https://git.apertis.org/cgit/backworth.git/).

## Updates to Apertis sites look & feel

During the cycle, Apertis websites were updated with a new visual and
matching colors, to improve visual experience. This included the wiki,
documentation and application developer portals.

In addition, the new visual is available for download form the
[marketing resources]( {{< ref "/marketing_resources.md" >}} ) page.

## Documentation improvements

[HotDoc](https://people.collabora.com/~meh/hotdoc_hotdoc/index.html) has
been significantly improved during the 16.06 cycle. We are currently
working on packaging these improvements, which will be available in
16.09.

The main improvements in this new version are:

  - Improved responsive theme, visible in HotDoc's documentation linked
    above
  - Implementation of "smart indexes", which mean HotDoc no longer
    requires symbols to be listed explicitly in the markdown pages
  - Implementation of autotools helpers, which make it easy to integrate
    HotDoc in our autotools-based projects, and no longer requires
    integrators to specify dependencies and installation rules for the
    HTML and Devhelp output

## Infrastructure

### Continous integration improvements

Starting in 16.03 all the Apertis specific packages (for example,
Mildenhall HMI) are automatically built in the Jenkins Continous
Integration system upon changes in `git` and are submitted to OBS on
success. During 16.06, this infrastructure was further improved and is
now also applied to various [non-Apertis specific
packages](https://git.apertis.org/cgit/packaging) which have Apertis
modifications (for example, AppArmor, D-Bus, GLib, Gtk, Pulseaudio and
systemd packaging).

Furthermore, on patch submission to Phabricator the same test builds are
run to provide early feedback to both submitters and reviewers.

### Updated packages

During this cycle, Apertis has been rebased on the latest Ubuntu 16.04
release (Xenial Xerus), updating the platform with new features as well
as latest bug and security fixes.

The rebasing started in early March when the first Ubuntu 16.04 beta1
was released and continued through the cycle, with the biggest changes
in late April after the final 16.04 release. The charts below show the
timeline of changes in packages for the target, development and sdk
repositories during the cycle.

|                                                                                    |                                                                                              |                                                                              |
| ---------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| ![350px|thumb|left|Target source packages](/images/16.06-target-trend.png) | ![350px|thumb|left|Development source packages](/images/16.06-development-trend.png) | ![350px|thumb|left|SDK source packages](/images/16.06-sdk-trend.png) |

### Shared Repositories

Repositories are found at:

` deb `<https://repositories.apertis.org/apertis/>` 16.06 target helper-libs development sdk hmi`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories. Note
that a string is added to package version depending on the distribution
suite based on. For example, if Trusty system is to be used expect to
install image-builder_7trusty1

|                                                                                                                           |                  |
| ------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| **Package**                                                                                                               | **Version**      |
| [image-builder, image-tools](https://git.apertis.org/cgit/image-utils.git/)                                               | 8.1              |
| [apertis-image-configs, apertis-image-scripts (\*)](https://git.apertis.org/cgit/apertis-image-customization.git/)        | 17               |
| [linaro-image-tools, python-linaro-image-tools](https://git.collabora.com/cgit/singularity/tools/linaro-image-tools.git/) | 2012.09.1-1co43  |
| parted, libparted0debian1                                                                                                 | 2.3-11ubuntu1co3 |
| python-debian                                                                                                             | \>=0.1.25        |

### Test Framework

Collabora's LAVA service triggers test cases to be run when image builds
succeed, the service is found at:

` `<https://lava.collabora.co.uk/>

The list of available test cases, including manual and automated, can be
found [here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
install, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation).

## Known issues

  - \- Failed to get MxLauncher in AppArmor tests

  - \- connman PAN connection fails with input/ouput error

  - \- apparmor-webkit-clutter: Several tests failed

  - \- Verify that Canterbury lockdown is working

  - \- apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail

  - \- bluez-phone A2DP Profile ain't working

  - \- bluez-hfp test fail

  - \- gstreamer buffering testcase fails if not run as the first
    testcase post flashing an image on target

  - \- WebKit2GTK+: fullscreen video animation regressed with the move
    to Wayland and no clutter-gtk, needs reimplementation

  - \- WebKit2GTK+: overriding of CSS for in-page elements needs
    reimplementation due to changes in upstream GTK+

  - \- WebKit2GTK+: drag and drop is broken due to the change in how
    Clutter and GTK+ interact now

  - \- gstreamer buffering testcase fails in SDK images
