+++
date = "2020-03-05"
weight = 100

title = "V2019.2 Release schedule"
+++

The v2019.2 release cycle started in January 2020.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2020-01-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2020-02-01     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2020-02-08     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2020-02-15     |
| RC testing                                                                                               | 2020-02-18..03-03 |
| v2019.2 release                                                                                          | 2020-03-05     |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
