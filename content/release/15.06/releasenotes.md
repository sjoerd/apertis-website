+++
date = "2015-09-21"
weight = 100

title = "15.06 ReleaseNotes"

aliases = [
    "/old-wiki/15.06/ReleaseNotes"
]
+++

# Apertis 15.06 Release

**15.06** is the current development distribution of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat API) and
Intel x86 (64/32-bit Intel) systems. Features planned for Apertis can be
found in the [Roadmap]( {{< ref "/roadmap.md" >}} ) page.

### What's available in the distribution?

The software stack in **15.06** is comprised of the following
technologies:

  - Linux 3.19
  - Graphical subsystem based on Xorg X server 1.17.1 and Clutter 1.20.0
    with full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.27, BlueZ 4.101
    and Ofono 1.14
  - Multimedia support provided by GStreamer 1.0.
  - The Telepathy framework, with XMPP and SIP support
  - The Folks contact management framework
  - The Clutter port of the WebKit browser engine with support for WebGL

### What's new in the distribution?

  - Migration to WebKit2GTK+
  - API review and submission workflow
  - Preferences and Persistence Design document
  - First phase of the transition from X11 to Wayland.

### Release downloads

| [Apertis 15.06 images](https://images.apertis.org/release/15.06/) |
| ----------------------------------------------------------------- |
| Intel 32-bit                                                      |
| Intel 64-bit / Minnowboard MAX                                    |

  - Apertis 15.06 repositories:

` deb `<https://repositories.apertis.org/apertis/>` 15.06 target development sdk hmi`

  - Apertis 15.06 infrastructure tools:

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

# Apertis 15.06 Release

This release builds on the Ubuntu 15.04 (Vivid) as base distribution.
The 15.06 release has been verified in the [15.06 test
report]().

## Migration to WebKit2GTK+

Work has been started on migrating from WebKit Clutter to WebKit2GTK+.
Since the port was still in its early stages, it was decided that the
best use of time was on pushing the migration forward in the source code
base rather than on integration, so there are no packages yet. Details
can be found on the [WebKit2]( {{< ref "/webkit2.md" >}} ) page, the code can be
found here:

<https://git.collabora.com/cgit/webkit-clutter.git/tree/?h=gtk-clutter>

## API review and submission workflow

The APIs for a number of Apertis core modules, such as the phone call
API, have undergone an initial stage of review. Changes as a result of
those reviews are being undertaken, and will continue into the next
release. These API reviews are [tracked in bug
reports](https://bugs.apertis.org/buglist.cgi?cmdtype=dorem&remaction=run&namedcmd=Open%20API%20reviews&sharer_id=16).

The following modules have had a preliminary review:

  - Thetford (vehicle information application) (*by e-mail*)
  - Walton (vehicle information manager) (*by e-mail*)
  - Welbourn (vehicle information simulator) (*by e-mail*)

The following modules have had a high-level review:

  - [Frampton (audio player
    application)](https://bugs.apertis.org/show_bug.cgi?id=211)
  - [libgrassmoor (media information and playback
    library)](https://bugs.apertis.org/show_bug.cgi?id=213)
  - [Mildenhall (UI widget
    library)](https://bugs.apertis.org/show_bug.cgi?id=217)
  - [Prestwood (disk mounter
    application)](https://bugs.apertis.org/show_bug.cgi?id=219)
  - [Tinwell (media playback
    service)](https://bugs.apertis.org/show_bug.cgi?id=222)
  - [libseaton (persistent data management
    library)](https://bugs.apertis.org/show_bug.cgi?id=215)
  - [Beckfoot (network interface management
    service)](https://bugs.apertis.org/show_bug.cgi?id=206)
  - [Shapwick (resource monitor
    application)](https://bugs.apertis.org/show_bug.cgi?id=221)
  - [libthornbury (UI utility
    library)](https://bugs.apertis.org/show_bug.cgi?id=216)
  - [Canterbury (application management and process control
    service)](https://bugs.apertis.org/show_bug.cgi?id=207)
  - [Chalgrove (application and service management
    service)](https://bugs.apertis.org/show_bug.cgi?id=208)
  - [libclapton (system information and logging
    library)](https://bugs.apertis.org/show_bug.cgi?id=212)
  - [Corbridge (Bluetooth management
    service)](https://bugs.apertis.org/show_bug.cgi?id=209)
  - [liblightwood (widget
    library)](https://bugs.apertis.org/show_bug.cgi?id=214)
  - [Barkway (system popup management and display
    service)](https://bugs.apertis.org/show_bug.cgi?id=205)
  - [Didcot (data sharing and file opening
    service)](https://bugs.apertis.org/show_bug.cgi?id=210)
  - [Ribchester (application
    mounter)](https://bugs.apertis.org/show_bug.cgi?id=220)
  - [Newport (download manager
    application)](https://bugs.apertis.org/show_bug.cgi?id=218)

The following APIs have had a more detailed review:

  - [Phone
    API](https://bugzilla.ecore.bosch.com/bugzilla/show_bug.cgi?id=3351)

## Applications Design document

The Applications document has been updated to use clearer terminology
(avoiding the term "application" in favour of either "application
bundle" or "graphical program", depending on context), and to recommend
standardizing on Android-style reversed DNS names such as
com.example.MyApp for all of the various internal names of bundles and
programs. Version 0.5.4 of the document is available here: [Apertis
Applications Design
v0.5.4](/images/apertis-applications-design-0.5.4.pdf). The
[Glossary]( {{< ref "/glossary.md" >}} ) has been updated accordingly.

## Multi-user Design document

The Multi-user design document has been split into two documents:

  - an "umbrella" document with terminology, comparisons with other
    platforms, and general recommendations for the topic of multi-user
    as a whole, but no specific use-cases for particular Apertis
    variants' models of how multiple users relate;
  - a document specific to transactional switching between users,
    describing use-cases and their proposed solutions for that model

These documents are currently under review and will be published during
the next quarterly release cycle.

## Preferences and Persistence Design document

A new design document has been produced, detailing Collabora’s
recommended approach for securely implementing app and user preferences,
and for storing persistent data to allow apps to save and restore their
state when restarted. This has undergone initial review, and will
continue to be discussed, and potentially implemented, during the next
release.

Version 0.2.0 of the document is available here: [Apertis Preferences
and Persistence Design
v0.2.0](/images/apertis-preferences-persistence-design-0.2.0.pdf).

## Security Design document

The Security design document has been updated to define terminology,
incorporate a new chapter on PolicyKit, describe the various security
boundaries present in the platform, and correct outdated information.
The updated version is currently under review and will be published
during the next quarterly release cycle.

## First phase of the transition from X11 to Wayland

The core HMI implementation has been ported from being specific to X11
to be display system agnostic, which means it can either run under X11
or Wayland. Furthermore the Mildenhall mutter plugin has been update to
the latest mutter release (3.16) to, again, be able to run both as an
X11 compositor and a Wayland display server. As this work was finalized
later in the quarter it was decided to not include it in the
distribution for 15.06, however the results of this work are available
in the apertis git repositories and will be made available in a proof of
concept image for the 64 bit intel platform shortly after the release
based on initial 15.09 images.

## Infrastructure

### Rebase on Ubuntu Vivid

During Q2 cycle, several activities have been carried out to be able to
rebase Apertis against new upstream base (Ubuntu Vivid Vervet). Over 292
packages have been updated in Target Platform, 408 packages in
Development Platform and 134 in SDK Platform.

### HMI Packages

HMI packages have also been updated to newer versions, and changes to
image tools to be able to accommodate variant builds.

### OBS Build Projects

At Collabora's OBS instance:

  - [OBS Apertis 15.06
    Target](https://build.collabora.co.uk/project/show?project=sac%3A15.06%3Atarget)
  - [OBS Apertis 15.06
    Development](https://build.collabora.co.uk/project/show?project=sac%3A15.06%3Adevelopment)
  - [OBS Apertis 15.06
    SDK](https://build.collabora.co.uk/project/show?project=sac%3A15.06%3Asdk)
  - [OBS Apertis 15.06
    HMI](https://build.collabora.co.uk/project/show?project=sac%3A15.06%3Ahmi)
  - [OBS Apertis Infrastructure
    Tools](https://build.collabora.co.uk/project/show?project=sac%3Ainfrastructure)

### Shared Repositories

Repositories are found at:

` deb `<https://repositories.apertis.org/apertis/>` 15.06 target development sdk hmi`

To be able to access those, a temporary user/passwd has been created
until properly publicly published: Username: **sac-dev** Password:
**Apa8Uo1mIeNgie6u**

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

*Use your Collabora credentials to access those until those get publicly
published.*

Image build tools can be found in the Apertis tools repositories. Note
that a string is added to package version depending on the distribution
suite based on. For example, if trusty system is to be used expect to
install image-builder_4trusty1

|                                               |                  |
| --------------------------------------------- | ---------------- |
| **Package**                                   | **Version**      |
| image-builder, image-tools                    | 5                |
| apertis-image-configs, apertis-image-scripts  | 12               |
| linaro-image-tools, python-linaro-image-tools | 2012.09.1-1co38  |
| parted, libparted0debian1                     | 2.3-11ubuntu1co3 |
| python-debian                                 | \>=0.1.25        |

### Test Framework

LAVA service at Collabora triggers test cases upon image builds, service
is found at:

` `<https://lava.collabora.co.uk/>

The list of available test cases, including those can be found
[here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
be able to install it, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation)

### Mailing List

A mailing list setup and configuration has been deferred for next
quarter release (15.09).

## Known issues

  - [lava sqlite-veryquick test partial fails]()
  - [apparmor-tracker: Test fails in ARM image with
    timeout]()
  - [cgroups-resource-control: Test times out for most
    platforms]()
  - [apparmor-folks: folks-alias-persistence and test-malicious
    failed]()
  - [webkit-clutter-javascriptcore: ecma/Date tests
    failing]()
