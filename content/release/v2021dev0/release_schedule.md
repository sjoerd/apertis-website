+++
date = "2019-11-21"
weight = 100

title = "V2021dev0 Release schedule"

aliases = [
    "/old-wiki/V2021dev0/Release_schedule"
]
+++

The v2021dev0 release cycle started in October 2019.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2019-10-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2019-11-22     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2019-11-29     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2019-12-06     |
| RC testing                                                                                               | 2019-12-09..19 |
| v2021dev0 release                                                                                        | 2019-12-20     |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
