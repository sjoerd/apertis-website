+++
date = "2020-06-19"
weight = 100

title = "v2020.2 Release Schedule"
+++

The v2020.2 release cycle started in July 2020.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2020-07-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2020-08-25     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2020-09-01     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2020-09-08     |
| RC testing                                                                                               | 2020-09-09..15 |
| v2020.1 release                                                                                          | 2020-09-16     |

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
