+++
date = "2019-11-21"
weight = 100

title = "V2019.1 Release schedule"

aliases = [
    "/old-wiki/V2019.1/Release_schedule"
]
+++

The v2019.1 release cycle started in October 2019.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2019-10-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2019-11-01     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2019-11-08     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2019-11-15     |
| RC testing                                                                                               | 2019-11-18..21 |
| v2019.1 release                                                                                          | 2019-11-22     |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
