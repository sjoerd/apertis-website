+++
date = "2020-09-15"
weight = 100

title = "V2021pre Release Schedule"

+++

The v2021pre release cycle started in October 2020.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2020-10-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2020-11-17     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2020-11-23     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2020-11-02     |
| RC testing                                                                                               | 2020-12-03..09 |
| v2021pre release                                                                                         | 2020-12-10     |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
