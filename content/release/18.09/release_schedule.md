+++
date = "2018-10-02"
weight = 100

title = "18.09 Release schedule"

aliases = [
    "/old-wiki/18.09/Release_schedule"
]
+++

The 18.09 release cycle starts in July 2018.

| Milestone                                                                                                | Date                  |
| -------------------------------------------------------------------------------------------------------- | --------------------- |
| Start of release cycle                                                                                   | 2018-07-01            |
| Soft feature freeze: end of feature proposal and review period                                           | 2018-07-10            |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2018-09-07            |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2018-09-12 EOD        |
| RC1 testing                                                                                              | 2018-09-12/2018-09-23 |
| 18.09 release                                                                                            | 2018-09-24            |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
