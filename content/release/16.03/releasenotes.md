+++
date = "2016-04-05"
weight = 100

title = "16.03 ReleaseNotes"

aliases = [
    "/old-wiki/16.03/ReleaseNotes"
]
+++

# Apertis 16.03 Release

**16.03** is the current stable development distribution of **Apertis**,
a Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat ABI) and
Intel x86 (64/32-bit Intel) systems. Features which are planned for
Apertis can be found on the [Roadmap]( {{< ref "/roadmap.md" >}} ) page.

### What's available in the distribution?

The software stack in **16.03** is comprised of the following
technologies:

  - Linux kernel 4.2
  - Graphical subsystem based on X.org X server 1.17.2 and Clutter
    1.24.2 with full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.27, BlueZ 5.35 and
    Ofono 1.17
  - Multimedia support provided by GStreamer 1.0
  - The Telepathy framework, with XMPP and SIP support
  - The Folks contact management framework
  - The Clutter port of the WebKit browser engine with support for WebGL
  - The GTK+ port of WebKit with features from WebKit Clutter ported to
    it and a ClutterActor wrapper
  - Scaled down Apertis image
  - Graphical subsystem image based on Wayland protocol 1.8.1

### What's new in the distribution?

  - Applications design update
  - Sensors and Actuators design update
  - Sensors and Actuators initial API preview
  - Geolocation and Navigation design update
  - Geocoding improvements
  - Status Framework design
  - Debug and Logging design
  - Text-to-speech design
  - Security design update
  - Automatic patch control using lint
  - Further updates to SDK libraries support for introspection
  - Performance improvements on WebKit2GTK+
  - Web runtime prototype
  - API documentation improvements, with support for DBus and better
    indexing
  - 25 components have been published under an open source license

### Release downloads

| [Apertis 16.03 images](https://images.apertis.org/release/16.03/) |
| ----------------------------------------------------------------- |
| Intel 32-bit                                                      |
| Intel 64-bit / Minnowboard MAX                                    |

#### Apertis 16.03 repositories

` deb `<https://repositories.apertis.org/apertis/>` 16.03 target helper-libs development sdk hmi`

#### Apertis 16.03 infrastructure tools

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

# Apertis 16.03 Release Notes

This release builds on the Ubuntu 15.10 (Wily) release as its base
distribution. The 16.03 release has been verified in the [16.03 test
report]().

## Applications design and implementation

Several topics related to the Applications design document have
progressed:

### Application namespacing

All of the standard application bundles included in Apertis images now
have a correctly-namespaced [reversed domain
name]( {{< ref "/glossary.md#reversed-domain-name" >}} ) such as
`org.apertis.Eye`.

### Application entry points

The [Application Entry Points]( {{< ref "application-entry-points.md" >}} )
design introduces metadata describing executable "entry points" into the
application bundle. These entry points include the user-visible
"applications" seen in launcher menus, application-switching user
interfaces and so on, as well as background services (agents). All of
the standard application bundles included in Apertis images include this
entry point metadata. The metadata format is heavily based on the
freedesktop.org Desktop Entry Specification used in open-source desktop
environments such as GNOME, with extensions to solve Apertis-specific
requirements; it supersedes the ad-hoc GSettings-based format previously
used for this.

The Canterbury application launcher and the Ribchester application mount
manager have been updated to read application entry points.

### Application bundle metadata

A preliminary [Application Bundle Metadata]( {{< ref "application-bundle-metadata.md" >}} )
design describes a simple metadata format for application bundles
themselves, as distinct from their entry points.

### Application Layout design and implementation

The relatively general outline of the structure of application bundles
in the [Applications design]( {{< ref "applications.md" >}} ) has been
expanded upon and made more specific in the new [Application
Layout]( {{< ref "application-layout.md" >}} ) design. This separates static
(read-only) application data from application-specific variable data,
and ensures that the directory hierarchy used in application bundles is
ready for multi-user systems. Implementation of this design in the
Ribchester mount manager and the Canterbury application launching
service is in progress, and will be completed for the 16.06 release,
with some of the changes already present in 16.03. In particular, the
Ribchester mount manager no longer changes the ownership of system-wide
directories like `/var` to the first user on the system.

The Canterbury application launching service now runs with its AppArmor
profile in "enforcing" mode, preventing it from launching application
bundles under its own privileges. In effect, this means that all
application bundles are required to have their own AppArmor profiles.

The Ribchester mount manager now has automated test coverage for
app-bundle management, and a number of bugs found by the new tests have
been fixed.

### Multi-user support

In preparation for multi-user systems, the Canterbury application
launching service now saves its state to a user-specific location.

### Conditional Access design

Detailed requirements for [Conditional
Access]( {{< ref "/designs/conditional_access/_index.md" >}} ) have been gathered, in
preparation for a design proposal to follow in a subsequent release
cycle.

## Sensors and Actuators design and implementation

Further improvements have been made to the [Sensors and Actuators
design]( {{< ref "sensors-and-actuators.md" >}} ), and implementation has started on
the first phase of the core functionality for the vehicle device daemon
it describes. An initial preview of the APIs it recommends has been
produced and made available for applications and third-party backends to
start being based around, although it is likely to change to some extent
in the future. Work will continue on this for the 16.06 release.

## Geolocation and Navigation design

Many changes have been made to the navigational parts of the
[Geolocation and Navigation design]( {{< ref "geolocation-and-navigation.md" >}} ), which
is now finalised and is [starting to be implemented
upstream](#geocoding-improvements). Implementation will
continue for the 16.06 release.

## Geocoding improvements

Parts of the geolocation design have started to be implemented in
upstream projects,
[GeoClue](https://www.freedesktop.org/wiki/Software/GeoClue/) and
[geocode-glib](https://developer.gnome.org/geocode-glib/stable/),
including limiting geocoding search results by radius, and geofencing
support. These are due to start being integrated in the 16.06 release.
All this work is being done and integrated upstream.

## Status Framework design

An initial draft of the [Status Framework
design]( {{< ref "concepts" >}} ) has been produced, and will be
developed further for the 16.06 release. It is a high-level design for
how applications can report status information, in a variety of
categories, to the ‘home screen’.

## Containerisation design

A high-level [containerisation design]( {{< ref "security.md#confining-applications-in-containers" >}} ) has
been produced, which explores various approaches to enforcing file
system and process namespaces on applications running on the system (all
of these systems are based around
[cgroups](https://en.wikipedia.org/wiki/Cgroups)). This work may be
researched and developed further in future to augment the existing
security systems in Apertis.

## Debug and Logging design

The [Debug and Logging design]( {{< ref "debug-and-logging.md" >}} ) has been
updated, and various of its recommendations have started to be
implemented, with the initial implementations planned to land in the
16.06 release, mostly focusing on ensuring consistent logging from all
parts of the system to the systemd journal, and allowing developers and
trusted parties access to those logs.

At the [GNOME developer experience
hackfest](https://wiki.gnome.org/Hackfests/DeveloperExperience2016), a
detailed plan for improving the [logging API in
GLib](https://developer.gnome.org/glib/stable/glib-Message-Logging.html)
was discussed and approved, and implementation has [started
upstream](https://bugzilla.gnome.org/show_bug.cgi?id=744456) on it. This
is intended to land in the 16.06 release, bringing support for
structured logging to GLib and improving its log handling API.

## Text to Speech design

The initial [Text to Speech design]( {{< ref "text-to-speech.md" >}} ) has been
expanded and finalised, and initial implementation has begun, including
adding new packages to the repositories (Pico TTS).

## Security design

Detailed requirements have been gathered for a [Compositor
security]( {{< ref "/compositor_security.md" >}} ) design proposal which is to
be worked on in a future release cycle.

The [Application Layout design and
implementation](#application-layout-design-and-implementation),
[Conditional Access design](#conditional-access-design)
and [Multi-user support](#multi-user-support) mentioned
above have also had security-related improvements.

## Linting

As part of the gradual improvements to the contribution process,
[Phabricator’s linting
support](https://secure.phabricator.com/book/phabricator/article/arcanist_lint/)
has been added to Canterbury as a testing ground, with the intention of
applying it to other modules next quarter. It automatically checks for
common problems with submitted patches, reducing the need for manual
review.

## GIR support in Apertis libraries

Continuing from the [work done on this in
15.12]( {{< ref "/release/15.12/releasenotes.md#gir-support-in-apertis-libraries" >}} ),
further work has been done to other Apertis libraries to ensure their
APIs are suitable for gobject-introspection (GIR). This is needed for
the hotdoc documentation tool and for exposing those SDK APIs in
languages such as JavaScript and Python, especially in the web runtime.
Work from the previous cycle and this cycle has landed for 16.03.

The modules affected during this cycle are: libwickham, Frome, Tinwell,
Prestwood, Newport, Frampton, Corbridge, Chalgrove, libseaton,
libgrassmoor, libclapton.

## WebKit2GTK+

The WebKit2 work continued, this time focusing on performance
improvements. Scrolling performance was improved by implementing a
feature called Async Scrolling, which unties the handling of scrolling
events from parsing and rendering so that both can be performed in
parallel instead of blocking on each other. This work has been made
available in the Apertis package, in addition to the git repository.

## Web Runtime

A [WebRuntime]( {{< ref "/webruntime.md" >}} ) design has been started this
quarter and a prototype was created to proof some of the concepts. The
prototype can be checked out from the following git repository:

<https://git.apertis.org/cgit/potshaft.git/>

A library is available that can make GObject libraries available in the
JavaScript context for WebKit2 WebViews using *seed*. Since seed is
unmaintained upstream, Collabora has picked it up and started an effort
to make it compatible with [gjs](https://wiki.gnome.org/Projects/Gjs), a
JavaScript binding for GNOME. This will ensure that the existing
codebase and documentation for using JavaScript with GNOME can be
leveraged. The existing code base, in particular, will be helpful for
testing purposes and for sample code.

Most of the effort is already available in the Apertis package. More
recent improvements are available upstream, and work in progress can be
obtained from Danilo Cesar's GitHub:

<https://github.com/danilocesar/seed/branches>

## Documentation

Improving on the [hotdoc package in
15.12]( {{< ref "/release/15.12/releasenotes.md#hotdoc:-new-api-documentation-tool" >}} ),
a new release of hotdoc, 0.7, has been packaged for use in 16.03.
Several libraries have been ported to use it instead of gtk-doc. The
D-Bus extension for hotdoc, which uses
[dbus-deviation](https://github.com/pwithnall/dbus-deviation), has also
been released and packaged for 16.03. It allows documenting D-Bus API
exposed by modules such as Canterbury. Work is ongoing to provide better
autotools integration and to create an API index usable by
[devhelp](https://wiki.gnome.org/Apps/Devhelp).

Modules ported during this cycle are: libgrassmoor, libclapton,
Canterbury, libthornbury.

## Module code and design review

After a thorough review, 25 components of the platform have been
published under an open source license. You can find the source code
repositories [here](https://git.apertis.org/cgit/).

The designs and code for libwickham (webview library) and libalton (maps
library), among others, have been reviewed and feedback has been
collected. Some of these improvements have been made already as part of
the [GIR support work](#gir-support-in-apertis-libraries)
and more is planned in the short term.

## Infrastructure

### Updated packages

During Q1 cycle, several activities have been carried out to be able to
update Apertis against the upstream base (Ubuntu Wily Werewolf). A total
of 400 fixes have been added to the target component, 1281 for the
development component and 518 for the SDK, adding new features and
fixing bugs, including security issues.

In addition to base distribution, a repository component for helper
libraries ("helper-libs") with the purpose of allowing target
applications to build on top of those and HMI packages have also been
updated to their latest version.

### OBS Build Projects

  - [OBS Apertis 16.03
    Target](https://build.collabora.co.uk/project/show?project=apertis%3A16.03%3Atarget)
  - [OBS Apertis 16.03 Helper
    Libraries](https://build.collabora.co.uk/project/show?project=apertis%3A16.03%3Ahelper-libs)
  - [OBS Apertis 16.03
    Development](https://build.collabora.co.uk/project/show?project=apertis%3A16.03%3Adevelopment)
  - [OBS Apertis 16.03
    SDK](https://build.collabora.co.uk/project/show?project=apertis%3A16.03%3Asdk)
  - [OBS Apertis 16.03
    HMI](https://build.collabora.co.uk/project/show?project=apertis%3A16.03%3Ahmi)
  - [OBS Apertis Infrastructure
    Tools](https://build.collabora.co.uk/project/show?project=apertis%3Ainfrastructure)

### Shared Repositories

Repositories are found at:

` deb `<https://repositories.apertis.org/apertis/>` 16.03 target helper-libs development sdk hmi`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Images are built with jobs driven by Jenkins, those are kept in a git
repository and can be uploaded to Jenkins master using
**jenkins-job-builder** tool.

` `<https://git.apertis.org/cgit/apertis-image-customization.git/tree/jenkins>

In addition to Jenkins jobs, some extra software is needed, listed in
the following table:

|                                                                                                          |                  |
| -------------------------------------------------------------------------------------------------------- | ---------------- |
| **Package**                                                                                              | **Version**      |
| [image-utils](https://git.apertis.org/cgit/image-utils.git/log/?h=16.03)                                 | 16.03            |
| [apertis-image-customization](https://git.apertis.org/cgit/apertis-image-customization.git/log/?h=16.03) | 16.03            |
| [linaro-image-tools](https://git.collabora.com/cgit/singularity/tools/linaro-image-tools.git/)           | 2012.09.1-1co42  |
| parted, libparted0debian1                                                                                | 2.3-11ubuntu1co3 |
| python-debian                                                                                            | \>=0.1.25        |

### Continuous Integration

The latest versions of various Apertis-hosted platform and HMI
components are automatically compiled on <https://jenkins.apertis.org/>
whenever a change is merged into the version control system, and any
compile-time automated tests that do not require special privileges are
run. This enables early detection of API breaks and other build issues.

To support this, most Apertis-hosted packages have been adapted to
compile in a standardized way.

### Test Framework

LAVA service at Collabora triggers test case runs on image builds, the
service is found at:

` `<https://lava.collabora.co.uk/>

The list of available test cases, can be found
[here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
be able to install it, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation).

## Known issues

  - [Bug 386]() - factory-reset-tool TC: flagcheck
    messages are hidden by Plymouth
  - [Bug 543]() - mxkineticscrollview-smooth-pan:Free
    scroll doesn't work
  - [Bug 673]() - Screen Blinking observed in the
    Webkit-contextual-zoom test case
  - [Bug 675]() - Platform update failed
  - [Bug 712]() - bluez-hfp test fail
  - [Bug 715]() - connman-pan-tethering:test fails
