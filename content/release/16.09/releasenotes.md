+++
date = "2016-10-07"
weight = 100

title = "16.09 ReleaseNotes"

aliases = [
    "/old-wiki/16.09/ReleaseNotes"
]
+++

# Apertis 16.09 Release

**16.09** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (ARMv7 using the hardfloat ABI) and
Intel x86-64 (64-bit) systems.

Since this release Intel x86 32-bit systems are no longer supported,
which includes devices like Zotac Z-BOX.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 16.09 release
are available in the [16.09 test
report]().

### Release downloads

| [Apertis 16.09 images](https://images.apertis.org/release/16.09/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |

The `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 16.09 repositories

` deb `<https://repositories.apertis.org/apertis/>` 16.09 target helper-libs development sdk hmi`

## New features

### Routing information sharing for navigation services

Traprain is a new library introduced in Apertis 16.09 allowing
navigation services to share routing information with third party
applications. It enables applications to get the available routes to
reach one or more destinations: for instance, with Traprain an
application can request the routes to the nearby restaurants and sort
them by estimated arrival time form the current location to help
choosing the one to book.

See the [Geolocation and navigation
design](https://docs.apertis.org/geolocation-and-navigation.html) for
details.

### Hardware accelerated support for Web contents

Apertis 16.09 ships with readily available support for accelerating Web
contents (CSS3 transforms and animations, WebGL scenes) using the GPU
efficiently under Wayland. This means that you can navigate the modern
Web even with limited CPU resources and run WebGL scenes fully
unleashing the power of your GPU.

Most of this work [has now been landed
upstream](https://blog.kov.eti.br/2016/09/webkitgtk-2-14-and-the-web-engines-hackfest/)
and we plan to submit the remaining optimizations soon.

### Canterbury API for launching preferences

A new API (`org.apertis.Canterbury.AppDbHandler.LaunchAppPreferences`)
has been implemented to allow applications to launch their preferences
in `mildenhall-settings`, including
[design](https://docs.apertis.org/preferences-and-persistence.html) and
support work, and unit tests for the implementation in Canterbury.

### Automatic debug packages

A full rebuild of every package in the archive has been carried out to
be able to provide automatically-generated debug packages in the Apertis
repositories: this means that every package in the archive which ships
binary executables or libraries now automatically has an associated
`-dbgsym` package that contains the debug symbols for those binaries,
making debugging with `gdb`, `valgrind`, `perf` and other development
tools much easier and consistent.

## Groundwork

### Web engine stabilization

The move to WebKit2GTK+ has been a [major improvement in Apertis
16.06]( {{< ref "/release/16.06/releasenotes.md#webkit2gtk.2b" >}} ): for this cycle we
focused on smoothing any rough edge and making it even more stable. We
fixed bugs in page rendering, touch events positioning, touch scrolling,
browser drawing loop, contextual zoom to provide a modern and fast Web
engine, fully integrated with the platform.

### JavaScript bindings to platform APIs

Apertis 16.09 ships a preview of the ability to use platform APIs from
native applications written in JavaScript using the Seed engine and
GObject-Introspection. This will enable application authors to write
their applications in pure JavaScript and sets the foundations for Web
applications based on standard W3C technologies that can have access to
the full capabilities of the platform.

### liblightwood 2 API review

A plan for submitting and reviewing the patches to change
[liblightwood](https://git.apertis.org/cgit/liblightwood.git) from
version 1 to version 2 (introducing new interface-based APIs) has been
created, and an initial round of review on the core work and a couple of
the ported widgets has been done. This work will continue into 16.12,
now that the groundwork has been laid.

### Memory Management

As test cases grows more sophisticated, improper memory management has
been detected in various points. To provide the most stable environment
even on long running session several memory management issues from leaks
to double-free has been fixed mostly in
[libthornbury](https://git.apertis.org/cgit/libthornbury.git) and
[mildenhall](https://git.apertis.org/cgit/mildenhall.git)

### mildenhall-settings refactoring

Common code that could share a single implementation has been moved from
each settings program to mildenhall-settings itself, improving the
separation between the UI drawing logic and the settings backend. This
reduces the chance of bugs, makes development easier and it's slightly
more efficient overall.

## Design

### List and UI customisation

Various updates have been made to the list design (not yet released) and
to the [UI customisation
design](https://docs.apertis.org/ui-customisation.html), to cover some
new use cases and to clarify how the list (or roller) interacts with
adapters and with models, so that objects are stored once in a model,
and can be displayed in multiple views simultaneously without being
duplicated in memory.

### Web runtime

Work has started to define a runtime to host Apertis applications
written using only HTML/CSS/JS: instead of C, C++ or Python,
applications can use HTML and CSS for all their UI needs, and JavaScript
for their logic. They can be fully local, without the need for an
Internet connection or use remote services just like any other Apertis
application. With the help of the [JavaScript
bindings](#javascript-bindings-to-platform-apis) previewed
in this release, applications using th web runtime will still be able to
access all the features provided by the platform.

We always want to make developing for Apertis as easy as possible, and
this is another step in that direction.

### Locale listing and switching

Minor updates have been made to the [internationalization
design](https://docs.apertis.org/internationalization.html) to clarify
the recommendation to use the [`systemd-localed` D-Bus
service](https://www.freedesktop.org/wiki/Software/systemd/localed/) for
determining and changing the system locale. Updates have been made
upstream to gettext to allow it to be used to extract and translate
strings from ClutterScript files, which are used for UI design.

## Infrastructure

### Continuous integration improvements

New packages have been added to the continuous integration loop, such as
`connman`, `appstream-glib`, `hotdoc`, `seed-webkit2`, `apertis-docs`,
and `apertis-designs`. See the [CI package
list](https://git.apertis.org/cgit/apertis-customizations.git/tree/development/jenkins/apertis-build-packaging.yaml#n57)
for the complete listing.

### Updated packages

The on-going baseline effort has lead to several base packages to be
updated: it is worth mentioning the addition of `appstream-glib` in the
`target` package set, the addition of the `syntax-highlighting`,
`devhelp`, and `license` extensions to `hotdoc` (already available
extensions such `C`, `dbus`, `gi`, `search` and `tag` have all been
upgraded to the latest upstream versions) to the `development` package
set, and the addition of development tools such `bmap-tools` for
speeding up the copying of images into boot media to the Apertis SDK.

Special attention has been put on packages composing the reference HMI,
all updated with the latest features and bugfixes. New packages such as
`traprain`, `apertis-docs`, `apertis-designs` have also been added to
Apertis.

### Apertis infrastructure tools

For Debian Jessie based systems:

` deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Test Framework

The list of available test cases, including manual and automated, can be
found [here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the Apertis tools repository. To
install, please follow
[instructions](https://lava.collabora.co.uk/static/docs/installing_on_debian.html#debian-installation).

## Known issues

  - ,  - Popups do not work

  - \- Software power button does not work on target

  - \- Factory reset does not work

  - \- mildenhall-settings: does not generate localization files from
    source

  - \- cgroups-resource-control: blkio-weights tests failed

  - \- Systemd ftbfs: tests failed

  - \- Target does not reboot after system update

  - \- Bluetooth pairing fails

  - \- Multitouch does not work with the Mildenhall compositor

  - \- No features are loaded in Settings application

  - \- PAN NAP testcase fails in the bluez-phone testcase

  - \- telepathy-gabble: Several tests failed

  - \- connman-usb-tethering Test case fails on SDK

  - \- libsoup-unit: ssl-test failed for ARM

Apparmor:

  - \- apparmor-session-lockdown-no-deny fails

  - \- apparmor-pulseaudio: ARM Failed to drain stream: Timeout

  - \- apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail

  - \- apparmor-session-lockdown-no-deny asserts that canterbury is
    running, but that isn't true on SDK

  - \- apparmor-session-lockdown-no-deny asserts that various app-bundle
    processes are running, but that doesn't work on LAVA

Web (WebKit, browser, GtkClutterLauncher, etc.):

  - \- webkit-clutter-javascriptcore: run-javascriptcore-tests fails

  - \- Status bar in browser does not get updated

  - \- Toggle button string is not visible in the browser

  - \- Not able to select input-box on-click in GtkClutterLauncher

  - \- Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - \- Browser crashes while running
    <http://scripty2.com/demos/touch/simple/>

  - \- PDF documents cannot be viewed on the browser

  - \- Cookies cannot be activated in the browser

  - \- Alert box pop up isn't coming up in browser

  - \- Youtube videos cannot be viewed on browser

  - \- Alignment issue in browser

  - \- Horizontal scroll is not working correctly in browser

Roller:

  - \- Items below to the expanded row cannot be selected

  - \- Items cannot be selected in Launcher

  - \- Focus lost during transitions

  - \- Issue with Mildenhall Roller tests

Eye (video player):

  - , , ,

Music application:

  - , , , ,
