+++
date = "2016-09-30"
weight = 100

title = "16.09 Release schedule"

aliases = [
    "/old-wiki/16.09/Release_schedule"
]
+++

The 16.09 release cycle started in July 2016.

<table>
<thead>
<tr class="header">
<th><p>Milestone</p></th>
<th><p>Date</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Start of release cycle</p></td>
<td><p>2016-07-22</p></td>
</tr>
<tr class="even">
<td><p>Soft feature freeze: end of feature proposal and review period</p></td>
<td><p>2016-08-30</p></td>
</tr>
<tr class="odd">
<td><p>Hard feature freeze: end of feature development for this release</p></td>
<td><p>2016-09-13</p></td>
</tr>
<tr class="even">
<td><p>Release candidate 1/hard code freeze: no new code changes may be made after this date</p></td>
<td><p>2016-09-27 EOD</p></td>
</tr>
<tr class="odd">
<td><p>16.09 release</p></td>
<td><p><del>2016-09-30</del><br />
2016-10-04<br />
<small>Delayed to include fix for <a href="https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1628687">CVE-2016-7795</a></small></p></td>
</tr>
</tbody>
</table>

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
