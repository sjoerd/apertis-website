+++
date = "2020-12-23"
weight = 100

title = "v2022dev1 Release schedule"
+++

The v2022dev1 release cycle started in January 2021.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2020-01-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2021-02-19        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2021-03-10        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2021-03-17        |
| RC testing                                                                                               | 2021-03-18..03-24 |
| v2022dev1 release                                                                                          | 2021-03-25        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
