+++
date = "2020-03-12"
weight = 100

title = "V2020.0 Release schedule"
+++

The v2020.0 release cycle started in February 2020.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2020-02-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2020-02-08     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2020-02-15     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2020-02-27     |
| RC testing                                                                                               | 2020-02-27..03-09 |
| v2020.0 release                                                                                          | 2020-03-12     |

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
