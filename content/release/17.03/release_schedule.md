+++
date = "2017-02-20"
weight = 100

title = "17.03 Release schedule"

aliases = [
    "/old-wiki/17.03/Release_schedule"
]
+++

The 17.03 preview release cycle starts in December 2016.

<table>
<thead>
<tr class="header">
<th><p>Milestone</p></th>
<th><p>Date</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Start of release cycle</p></td>
<td><p>2016-12-19</p></td>
</tr>
<tr class="even">
<td><p>Feature proposal freeze: end of feature proposal and review period<br />
New features must be proposed, reviewed and accepted by this date. Any features which are not accepted by this date will need to be proposed for a future release.</p></td>
<td><p><s>2017-02-17</s><br />
2017-02-24</p></td>
</tr>
<tr class="odd">
<td><p>Soft code freeze: end of feature development for this release<br />
From this date onwards, only bug fixes should be accepted.</p></td>
<td><p>2017-03-10</p></td>
</tr>
<tr class="even">
<td><p>Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date</p></td>
<td><p>2017-03-21 EOD</p></td>
</tr>
<tr class="odd">
<td><p>RC1 testing</p></td>
<td><p>2017-03-22/2017-03-23</p></td>
</tr>
<tr class="even">
<td><p>17.03 release</p></td>
<td><p>2017-03-24</p></td>
</tr>
</tbody>
</table>

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
