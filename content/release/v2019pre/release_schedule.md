+++
date = "2019-05-23"
weight = 100

title = "V2019pre Release schedule"

aliases = [
    "/old-wiki/V2019pre/Release_schedule"
]
+++

The v2019pre release cycle started in April 2019.

<table>
<thead>
<tr class="header">
<th><p>Milestone</p></th>
<th><p>Date</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Start of release cycle</p></td>
<td><p><s>2019-04-01</s><br />
2019-04-15</p></td>
</tr>
<tr class="even">
<td><p>Soft feature freeze: end of feature proposal and review period</p></td>
<td><p>2019-05-21</p></td>
</tr>
<tr class="odd">
<td><p>Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed</p></td>
<td><p>2019-06-04</p></td>
</tr>
<tr class="even">
<td><p>Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date</p></td>
<td><p>2019-06-11 EOD</p></td>
</tr>
<tr class="odd">
<td><p>RC testing</p></td>
<td><p>2019-06-12..25</p></td>
</tr>
<tr class="even">
<td><p>v2019pre release</p></td>
<td><p>2019-06-26</p></td>
</tr>
</tbody>
</table>

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
