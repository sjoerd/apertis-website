+++
date = "2019-08-22"
weight = 100

title = "V2020dev0 Release schedule"

aliases = [
    "/old-wiki/V2020dev0/Release_schedule"
]
+++

The v2020dev0 release cycle started in July 2019.

| Milestone                                                                                                | Date           |
| -------------------------------------------------------------------------------------------------------- | -------------- |
| Start of release cycle                                                                                   | 2019-07-01     |
| Soft feature freeze: end of feature proposal and review period                                           | 2019-08-21     |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2019-09-04     |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2019-09-11 EOD |
| RC testing                                                                                               | 2019-09-12..25 |
| v2020dev0 release                                                                                        | 2019-09-26     |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
