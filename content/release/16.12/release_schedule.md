+++
date = "2016-11-16"
weight = 100

title = "16.12 Release schedule"

aliases = [
    "/old-wiki/16.12/Release_schedule"
]
+++

The 16.12 release cycle starts in October 2016.

| Milestone                                                                                   | Date                  |
| ------------------------------------------------------------------------------------------- | --------------------- |
| Start of release cycle                                                                      | 2016-10-03            |
| Soft feature freeze: end of feature proposal and review period                              | 2016-11-16            |
| Hard feature freeze: end of feature development for this release                            | 2016-11-30            |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date | 2016-12-13 EOD        |
| RC1 testing                                                                                 | 2016-12-14/2016-12-15 |
| 16.12 release                                                                               | 2016-12-16            |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - [Roadmap]( {{< ref "/roadmap.md" >}} )
  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
