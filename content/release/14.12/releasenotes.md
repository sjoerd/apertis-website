+++
date = "2015-11-30"
weight = 100

title = "14.12 ReleaseNotes"

aliases = [
    "/old-wiki/14.12/ReleaseNotes"
]
+++

# Secure Automotive Cloud 14.12 Release

**14.12** is the current development distribution of **Secure Automotive
Cloud (SAC)**, a Debian/Ubuntu derivative distribution geared towards
the creation of product-specific images for ARM (ARMv7 using the
hardfloat API) and Intel i386 (32-bit Intel) systems. Features planned
for SAC can be found in the [Roadmap]( {{< ref "/roadmap.md" >}} ) page.

### What's available in the distribution?

The software stack in **14.12** is comprised of the following
technologies:

  - Linux 3.16
  - Graphical subsystem based on Xorg X server 1.16.0 and Clutter 1.18.4
    with full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.21, BlueZ 4.101
    and Ofono 1.14
  - Multimedia support provided by GStreamer 1.0.
  - The Telepathy framework, with XMPP and SIP support
  - The Folks contact management framework
  - The Clutter port of the WebKit browser engine with support for WebGL

### What's in the distribution?

This release focused the work mostly on setting up infrastructure and
updating documentation in preparation for a public release.

  - Public wiki
  - B\&I services
      - OBS
      - Git repositories
      - images and packages repositories
      - LAVA server
  - Source Code Readiness
  - Updated Concept Designs
  - Updated Feature Documentation
  - Updated Test Case Documentation
  - Evaluation of Wayland
  - Evaluation of Webkit2-GTK+

### Release downloads

| [SAC 14.12 images](https://images.secure-automotive-cloud.org/release/14.12/) |
| ----------------------------------------------------------------------------- |
| Intel 32-bit                                                                  |

  - SAC 14.12 repositories:

` deb `<https://repositories.secure-automotive-cloud.org/sac/>` 14.12 target development sdk hmi`

  - SAC 14.12 infrastructure tools:

` deb `<https://repositories.collabora.co.uk/debian/>` jessie tools`
` deb `<https://repositories.secure-automotive-cloud.org/debian/>` jessie-14.12 tools`

# SAC 14.12 Release

This release builds on the Ubuntu 14.10 (Utopic Unicorn) as base
distribution. The 14.12 release has been verified in the [14.12 test
report]().

## Source Code Readiness

Ongoing work to ensure the SAC source code is ready to be published with
an open source licensed.

When ready, it will be available at the [Source Code
Repositories](https://git.secure-automotive-cloud.org).

## Webkit2GTK+ Evaluation

  - Moving to WebKit2GTK+ evaluation

## Wayland Evaluation

  - [Moving to Wayland (evaluation
    document)]( {{< ref "/waylandevaluation/_index.md" >}} )

## Concept Designs

This is a set of technical documents comprising many aspects of the
project, written through the history of the project, taking into account
the situation of each field at that time. Information in these documents
may be outdated, but it is nonetheless import to provide context for the
decision making process and overall vision of the project.

The designs are available [here](https://designs.apertis.org/).

## Feature Documentation

Updated documentation for the features developed during the project.

[Here]() is a list of the available
documentation.

## Build and Integration

During Q4 cycle, several activities have been carried out to be able to
set all SAC infrastructure in place. Most interesting remarks follow
below:

### Distribution Bootstrap

Imported into Collabora's OBS a snapshot of Krishna distribution, while
being in development, synced it against latest Ubuntu Utopic (14.10)
release:

  - Remove target dependencies on GPLv3 GNU GMP. (In target runtime,
    every package needs that needs GMP needs to be linked against
    libgmp3c2 instead libgmp10, per project license requirements. Note
    that latest GNU GMP (providing libgmp10) is still needed by GNU GCC
    toolchain)
  - Update for newer dpkg, which needed latest GCC toolchain (gcc-4.9,
    binutils, glibc, ...)
  - Update toolchain default to gcc-4.9
  - Update Linux kernel to 3.16 (as Ubuntu Utopic ships)
  - Re-include lost delta on libdrm (needs to build libkms)
  - Transition to newer Perl ABI from 5.18 to 5.20
  - Transition to newer Ruby ABI from 1.9.3 to 2.1
  - Merge latest LibreOffice in Ubuntu Utopic
  - Update maven Java packages to latest versions
  - Update Haskell (ghc) packages
  - Transition to newer boost1.55
  - Source package removals/inclusion:
      - gccgo-4.9 - build now by gcc-4.9
      - add new cross compiler version, gcc-4.9-armhf-cross
      - remove Linux and U-Boot Chaiwala packages
  - Package updates and merges:
      - Several build fixes, including: glibmm2.4, subversion, xen, etc.
      - Shuffle several packages to satisfy dependencies from
        development to target; and from sdk to development.

### Build Projects

At Collabora's OBS instance, additional component was added for
UI/Application packages, HMI. As well as keeping Target, Development and
SDK components, and Tools built against Debian Jessie.

  - [OBS SAC 14.12
    HMI](https://build.collabora.co.uk/project/show?project=sac%3A14.12%3Ahmi)
  - [OBS SAC 14.12
    Target](https://build.collabora.co.uk/project/show?project=sac%3A14.12%3Atarget)
  - [OBS SAC 14.12
    Development](https://build.collabora.co.uk/project/show?project=sac%3A14.12%3Adevelopment)
  - [OBS SAC 14.12
    SDK](https://build.collabora.co.uk/project/show?project=sac%3A14.12%3Asdk)
  - [OBS SAC 14.12
    Tools](https://build.collabora.co.uk/project/show?project=sac%3A14.12%3Atools)

### Shared Repositories

Repositories are found at:

` deb `<https://repositories.secure-automotive-cloud.org/sac/>` 14.12 target development sdk hmi`

` deb `<https://repositories.secure-automotive-cloud.org/debian/>` jessie-14.12 target development sdk hmi`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.secure-automotive-cloud.org/>

*Use your Collabora credentials to access those until those get publicly
published.*

### Test Framework

LAVA service at Collabora triggers test cases upon image builds, service
is found at:

` `<https://lava.collabora.co.uk/>

#### Test Cases

A list of 120 Test Cases have been added to the wiki. This also includes
several templates for test case development and required documentation
for different test case steps.

The list of available test cases can be found
[here]( {{< ref "/qa/test_cases" >}} ).

### Issue Tracker

A issue tracker for SAC has been deployed and configured at:

` `<https://bugs.secure-automotive-cloud.org/>

### Wiki

A wiki has been deployed and populated with updated documentation for
SAC

` `<https://secure-automotive-cloud.org/>

### Mailing List

A mailing list setup and configuration has been deferred for next
quarter release (15.03).

## Known issues

No known issues at the moment. Tests are still in progress.
