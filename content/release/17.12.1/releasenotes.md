+++
date = "2018-02-05"
weight = 100

title = "17.12.1 ReleaseNotes"

aliases = [
    "/old-wiki/17.12.1/ReleaseNotes"
]
+++

# Apertis 17.12.1 Release

**17.12.1** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both in the 32bit ARMv7 version using
the hardfloat ABI and in the 64-bit ARMv8 version) and Intel x86-64
(64-bit) systems.

This Apertis release is a minor update over Apertis 17.12.0, and is
based on top of the Ubuntu 16.04 (Xenial) LTS release with several
customization. Test results of the 17.12.1 release are available in the
[17.12.1 test report]().

### Release downloads

| [Apertis 17.12 images](https://images.apertis.org/release/17.12/) |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |
| ARM 32-bit                                                        |
| ARM 64-bit                                                        |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 17.12 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 17.12 target helper-libs development sdk hmi`

## Bug fixes

  - Voice/Audio is not heard for the Bluez-hfp profile in i.MX6

  - Remove bash version 4.3 from minimal and target images

  - Cross-compiling fails using ADE on AMD64 SDK

  - No music and Video files are present in the image (issue in the
    Mildenhall HMI triggered by the new images)

## Breaks

### Bash removal from `minimal` and `target` images

The Bash shell has been relicensed under GPLv3 so it is now no longer
shipped on `minimal` and `target` images, as those are meant to provide
a reference for deployments where some of the GPLv3 provisions can be
excessively challenging to fulfill. This means that scripts relying on
`/bin/bash` being available or `/bin/sh` supporting Bash-specific
features won't work anymore. Those scripts need to be updated to use
`/bin/sh` (which is now provided by [the Dash
shell](http://manpages.ubuntu.com/manpages/xenial/en/man1/dash.1.html))
and should only rely on POSIX shell features.

## Infrastructure

### Apertis infrastructure tools

For Debian Jessie based systems:

` $ deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` $ deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

## Known issues

### Normal (139)

  - : Not able to load heavy sites on GtkClutterLauncher

  - : No connectivity Popup is not seen when the internet is
    disconnected.

  - : remove INSTALL, aclocal.m4 files from langtoft

  - : apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - : factory-reset-tool TC: flagcheck messages are hidden by Plymouth

  - : libgrassmoor: executes tracker-control binary

  - : mildenhall-settings: does not generate localization files from
    source

  - : cgroups-resource-control: blkio-weights tests failed

  - : Target doesn't reboot after system update

  - : Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - : telepathy-gabble: Several tests failed

  - : libsoup-unit: ssl-test failed for ARM

  - : Interaction with PulseAudio not allowed by its AppArmor profile

  - : Fix folks EDS tests to not be racy

  - : GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - : factory reset with a different image's rootfs.tar.gz results in
    emergency mode

  - : shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - : gupnp-services: test service failed

  - : Crash when initialising egl on ARM target

  - : Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - : Mismatch between server version file and sysroot version

  - : Focus in launcher rollers broken because of copy/paste errors

  - : Test apps are failing in Liblightwood with the use of GTest

  - : Unusable header in Traprain section in Devhelp

  - : Clang package fails to install appropriate egg-info needed by
    hotdoc

  - : tracker-indexing-local-storage: Stopping tracker-store services

  - : apparmor-tracker: underlying_tests failed

  - : VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - : VirtualBox freezes using 100% CPU when resuming the host from
    suspend

  - : Canterbury messes up kerning when .desktop uses unicode chars

  - : Ribchester: deadlock when calling RemoveApp() right after
    RollBack()

  - : make check fails on libbredon package for wayland warnings

  - : polkit-parsing: TEST_RESULT:fail

  - : Cannot open links within website like yahoo.com

  - : tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - : apparmor-folks: unable to link contacts to test unlinking

  - : mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - : libgles2-vivante-dev is not installable

  - : telepathy-gabble-tests should depend on python-dbus

  - : gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - : Confirm dialog status updated before selecting the confirm option
    YES/NO

  - : webview Y offset not considered to place, full screen video on
    youtube webpage

  - : cgroups-resource-control: test network-cgroup-prio-class failed

  - : Search feature doesn't work correctly for appdev portal located at
    <https://appdev.apertis.org/documentation/index.html>

  - : GObject Generator link throws 404 error

  - : GLib, GIO Reference Manual links are incorrectly swapped

  - : folks: random tests fail

  - : Album art is missing in one of the rows of the songs application

  - : Canterbury entry-point launching hides global popups, but only
    sometimes

  - : <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - : Segmentation fault when disposing test executable of mildenhall

  - : The web runtime doesn't set the related view when opening new
    windows

  - : Deploying LAVA tests for SDK images on personal stream fails

  - : Cross debugging through GDB and GDBserver Not possible.

  - : GtkClutterLauncher: Segfaults with mouse right-clicking

  - : "didcot-client: autopkgtest fails with
    org.apertis.Didcot.Error.NoApplicationFound and ""Unit
    didcot.service could not be found"""

  - : rhosydd: integration test fails

  - : ribchester: gnome-desktop-testing test times out

  - : canterbury: Most of the tests fail

  - : Status bar is not getting updated with the current song/video
    being played

  - : Compositor hides the other screens

  - : Variable roller is not working

  - : In mildenhall, URL history speller implementation is incomplete.

  - : MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - : arm-linux-gnueabihf-pkg-config does not work with sysroots
    installed by \`ade\`

  - : libshoreham packaging bugs

  - : libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - : rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - : Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - : MildenhallSelPopupItem model should be changed to accept only
    gchar \* instead of MildenhallSelPopupItemIconDetail for icons

  - : Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data
    package versions in the archive

  - : libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - : webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - : bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - : Shutdown not working properly in virtualbox

  - : virtual keyboard is not showing for password input field of any
    webpage

  - : Steps like pattern is seen in the background in songs application

  - : Avoid unconstrained dbus AppArmor rules in frome

  - : Newport test fails on minimal images

  - : webkit2GTK crash observed flicking on webview from other widget

  - : Mildenhall should install themes in the standard xdg data dirs

  - : Page rendering is not smooth in sites like www.yahoo.com

  - : HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - : Render theme buttons are not updating with respect to different
    zoom levels

  - : Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - : Frampton application doesn't load when we re-launch them after
    clicking the back button

  - : Observing multiple service instances in all 17.06 SDK images

  - : Incorrect network available state shown in GNetworkMonitor

  - : Crash observed on webruntime framework

  - : Containers fail to load on Gen4 host

  - : Crash observed on seed module when we accesing the D-Bus call
    method

  - : introspectable support for GObject property, signal and methods

  - : gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - : Segmentation fault occurs while exectuing
    webkit2gtk-aligned-scroll test case

  - : zoom feature is not working as expected

  - : Preseed action is needed for Debos

  - : Debos: allow to use partition number for 'raw' action

  - : Debos: mountpoints which are mounted only in build time

  - : Debos: option for kernel command parameters for
    'filesystem-deplay' action

  - : ribchester-core causes apparmor denies on non-btrfs minimal image

  - : Debos: changes 'resolv.conf' during commands execution chroot

  - : Volume Control application hangs on opening it

  - : No splashscreen on boot

  - : Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - : Blank screen seen on executing last-resort-message testcase

  - : Boot chart doesn't get generated

  - : folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - : folks-telepathy: folks-retrieve-contacts-telepathy_sh.service
    failed

  - : folks-alias-persistence: folks-alias-persistence_sh.service
    failed

  - : folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - : folks-search-contacts: folks-search-contacts_sh.service failed

  - : apparmor-tracker: AssertionError: False is not true

  - : apparmor: /usr/lib/apparmor/tests/environ fails

  - : apparmor-folks: Several tests fail

  - : apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - : gupnp-services: browsing and introspection tests fail

  - : tracker-indexing-local-storage: AssertionError: False is not true

  - : didcot: test_open_uri: assertion failed

  - : sqlite: Many tests fail for target amd64, armhf and sdk

  - : dbus-installed-tests: service failed because a timeout was
    exceeded

  - : boot-no-crashes: ANOM_ABEND found in journalctl logs

  - : tracker-indexing-mass-storage test case fails

  - : traprain: sadt: error: cannot find debian/tests/control

  - : canterbury: core-as-root and full-as-root tests failed

  - : ribchester: Job for generated-test-case-ribchester.service
    canceled

  - : do-branching fails at a late stage cloning OBS binary repos

  - : frome: \`/tmp/frome-\*': No such file or directory

  - : apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - : check-dbus-services: Error:
    GDBus.Error:org.freedesktop.DBus.Error.AccessDenied

  - : Harbormaster always fail with commit message error

  - : A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - : Full-screen Mode fails in Apertis SDK 17.12 in VirtualBox

  - : VirtualBox 5.2 Guest Additions causing backtrace and stalling of
    boot

  - : Apertis SDK fails to shutdown cleanly.

  - : webkit2gtk-drag-and-drop doesn't work with touch

  - : SDK hangs when trying to execute bluez-hfp testcase

  - : OSTree testsuite hangs the gouda Jenkins build slave

  - : youtube content is not rendering on GtkClutterLauncher window

  - : traffic-control-tcmmd test fails

### Low (46)

  - : Upstream: linux-tools-generic should depend on lsb-release

  - : Mildenhall compositor crops windows

  - : Documentation is not available from the main folder

  - : Power button appers to be disabled on target

  - : Network search pop-up isn't coming up in wi-fi settings

  - : mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - : Clutter_text_set_text API redraws entire clutterstage

  - : "bluetooth device pair failing with ""Invalid arguments in method
    call"""

  - : Videos are hidden when Eye is launched

  - : Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - : Video doesn't play when toggling from full screen to detail view

  - : Simulator screen is not in center but left aligned

  - : Bluetooth pairing option not working as expected

  - : Back option is missing as part of the tool tip

  - : The video player window is split into 2 frames in default view

  - : Horizontal scroll is not shown on GtkClutterLauncher

  - : The background HMI is blank on clicking the button for Power OFF

  - : Only half of the hmi is covered when opening gnome.org

  - : Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - : Background video is not played in some website with
    GtkClutterLauncher

  - : Drop down lists are not working on a site like facebook

  - : Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - : Zoom in feature does not work on google maps

  - : Printing hotdoc webpage directly results in misformatted document

  - : Context drawer displced when switching to fullscreen in browser
    app

  - : Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - : beep audio decoder gives errors continously

  - : If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - : Album Art Thumbnail missing in Thumb view in ARTIST Application

  - : gstreamer1-0-decode: Failed to load plugin warning

  - : Compositor seems to hide the bottom menu of a webpage

  - : Spacing issues between text and selection box in website like
    amazon

  - : Content on a webpage doesn't load in sync with the scroll bar

  - : Resizing the window causes page corruption

  - : qemu-arm-static not found in the pre installed qemu-user-static
    package

  - : Blank screen seen in Songs application on re-entry

  - : Songs do not start playing from the beginning but instead start a
    little ahead

  - : Roller problem in settings application

  - : "connman: patch ""Use ProtectSystem=true"" rejected upstream"

  - : "connman: patch ""device: Don't report EALREADY"" not accepted
    upstream"

  - : On multiple re-entries from settings to eye the compositor hangs

  - : On first flashing of the image any second application does not get
    displayed (eg. album art)

  - : Segmentation fault is observed on closing the
    mildenhall-compositor

  - : Inital Roller mappings are misaligned on the HMI

  - : ade process fails to start gdbserver if a first try has failed

  - : Remove unnecessary folks package dependencies for automated tests

### Lowest (2)

  - : Failed to load Captcha in Apertis developer portal

  - : Broken HTML generation in Backworth for the developers portal
    website
