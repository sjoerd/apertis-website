#!/usr/bin/python3

import sys

from pandocfilters import toJSONFilter, Para, Link, Note, Str, stringify

def footnotify(key, value, format_, meta):
  """
  For each link add a footnote with the link URL, useful when the document is printed.
  """
  if key == "Link":
    _, txt, (url, title_text) = value
    empty_attrs = ["", [], []]
    # Ignore bare links like <http://example.com>; otherwise we run into a
    # recursive problem where Pandoc tries to apply this same filter to the
    # new link in the footnote. Also ignore URLs that begin with "#", since
    # those point to sections within the document.
    if stringify(txt) != url and not url.startswith("#"):
      empty_attrs = ["", [], []]
      footnote_link = Link(empty_attrs, [Str(url)], [url, ""])
      return [Link(*value), Note([Para([footnote_link])])]

if __name__ == "__main__":
  toJSONFilter(footnotify)
